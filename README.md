## What is this repository for?

This application is my primitive implementation of Ukrainian embroidery shop. I have developed flexible REST APIs to work with such essences as Product, Product Type, Producer, User and Cart. I also developed UI to perform CRUD operations with Product, Cart and User.

Project is written on Java - cross-platform language using such web frameworks as Spring(IoC, MVС, Security) and JPA(Hibernate). I use Maven as build tool and PostgreSQL as relational database. I use Liquibase as open source database-independent library for tracking, managing and applying database schema changes. I use Maven Checkstyle and Findbugs plugins for automatic code validation. Also I use SLF4J and Log4J2 as logging frameworks. At the end, I use Bootstrap to create responsive design.

## How do I get set up?

You should do few simple things.

* Clone repository (git clone git@bitbucket.org:OlehZanevych/shop.git)

* Create database on PostgreSQL using required properties from root pom.xml. This installation by default looks like:
```sh
$ sudo apt-get install postgresql
$ sudo -u postgres psql
$ \password
	> now enter password 'postgres' two times (default password)
$ CREATE DATABASE shop;
$ \q
```

* Building project (mvn clean install)

* Initialize database schema (go into shop-db-structure and run mvn liquibase:update)

* Seed database with test data (go into shop-sql and run mvn -Pdata install)

* Run shop-rest-api on Server (or deploy shop-rest-api.war)

## P.S.

Main page has such http://localhost:8080/shop-rest-api/api/main strange URL.

You can try login, entering the password "test" and one of the following logins: "customer", "moderator", "admin", "ban".