package org.shop.facade.facade;

/**
 * Common interface for Get and Update facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 */
public interface EditFacade<RESOURCE> extends ReadFacade<RESOURCE> {
	
	/**
	 * Method for updating entity.
	 * @param id id
	 * @param resource
	 */
	void updateResource(final Long id, RESOURCE resource);

}
