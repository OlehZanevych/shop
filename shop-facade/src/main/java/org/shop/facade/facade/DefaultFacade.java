package org.shop.facade.facade;

import org.shop.converter.AbstractResourceConverter;
import org.shop.converter.BaseConverter;
import org.shop.model.ModelWithSimpleId;
import org.shop.resource.APIResource;
import org.shop.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of all facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <SERVICE> Service class.
 * @param <ENTITYCONVERTER> Entity converter class.
 * @param <RESOURCECONVERTER> Resource converter class.
 */
@Transactional
public class DefaultFacade<ENTITY extends ModelWithSimpleId, RESOURCE extends APIResource,
		SERVICE extends Service<ENTITY>, ENTITYCONVERTER extends BaseConverter<ENTITY, RESOURCE>,
		RESOURCECONVERTER extends AbstractResourceConverter<RESOURCE, ENTITY>>
		extends DefaultEditFacade<ENTITY, RESOURCE, SERVICE, ENTITYCONVERTER, RESOURCECONVERTER>
		implements Facade<RESOURCE> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFacade.class);

	@Override
	public RESOURCE createResource(final RESOURCE resource) {
		LOGGER.info("Creating resource: {}", resource);
		
		ENTITY entity = resourceConverter.convert(resource);

		service.createEntity(entity);

		return entityConverter.convert(entity);
	}

	@Override
	public void removeResource(final Long id) {
		LOGGER.info("Removing resource with id: {}", id);
		
		service.removeEntity(id);
	}
	
}
