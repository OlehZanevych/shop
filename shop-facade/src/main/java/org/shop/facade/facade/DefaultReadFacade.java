package org.shop.facade.facade;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.shop.converter.BaseConverter;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.pagination.PagedResult;
import org.shop.resource.APIResource;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.service.ReadService;
import org.shop.service.session.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of Get facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <SERVICE> Service class.
 * @param <ENTITYCONVERTER> Entity converter class.
 */
@Transactional
public class DefaultReadFacade<ENTITY extends ModelWithSimpleId, RESOURCE extends APIResource,
		SERVICE extends ReadService<ENTITY>, ENTITYCONVERTER extends BaseConverter<ENTITY, RESOURCE>>
		implements ReadFacade<RESOURCE> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultReadFacade.class);
	
	protected SERVICE service;
	
	protected ENTITYCONVERTER entityConverter;
	
	@javax.annotation.Resource(name = "defaultSessionService")
	protected SessionService sessionService;
	
	@Override
	public RESOURCE getResource(final Long id, final Request request) {
		LOGGER.info("Getting resource with id: {}", id);
		
		return entityConverter.convert(service.getEntity(id, request));
	}
	
	@Override
	public PagedResultResource<RESOURCE> getResources(final PagedRequest request) {
		LOGGER.info("Getting paged result resource");
		
		PagedResult<ENTITY> pagedResult = service.getEntities(request, sessionService.getUserId());

		List<RESOURCE> resources = entityConverter.convertAll(pagedResult.getEntities());

		return new PagedResultResource<>(pagedResult.getOffset(), pagedResult.getLimit(), pagedResult.getCount(), resources);
	}
	
	@Override
	public List<RESOURCE> getResourcesList(final PagedRequest request) {
		LOGGER.info("Getting resources list");
		
		return entityConverter.convertAll(service.getEntitiesList(request, sessionService.getUserId()));
	}
	
	@Override
	public Map<Long, RESOURCE> getResourcesMap(final PagedRequest request) {
		LOGGER.info("Getting resources map");
		
		return getResourcesList(request).stream().collect(Collectors.toMap(APIResource::getId, Function.identity()));
	}

	public SERVICE getService() {
		return service;
	}

	@Required
	public void setService(final SERVICE service) {
		this.service = service;
	}

	public ENTITYCONVERTER getEntityConverter() {
		return entityConverter;
	}

	@Required
	public void setEntityConverter(final ENTITYCONVERTER entityConverter) {
		this.entityConverter = entityConverter;
	}

}
