package org.shop.facade.facade;

import java.util.List;
import java.util.Map;

import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;

/**
 * Common interface for Get facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 */
public interface ReadFacade<RESOURCE> {
	
	/**
	 * Method for getting resource.
	 * @param id id
	 * @param request Request
	 * @return Entity resource.
	 */
	RESOURCE getResource(Long id, Request request);
	
	/**
	 * Method for getting paged result for resources.
	 * @param request PagedRequest
	 * @return paged result
	 */
	PagedResultResource<RESOURCE> getResources(PagedRequest request);
	
	/**
	 * Method for getting resources list.
	 * @param request PagedRequest
	 * @return resources list
	 */
	List<RESOURCE> getResourcesList(PagedRequest request);
	
	/**
	 * Method for getting resources map.
	 * @param request PagedRequest
	 * @return resources map
	 */
	Map<Long, RESOURCE> getResourcesMap(PagedRequest request);
}
