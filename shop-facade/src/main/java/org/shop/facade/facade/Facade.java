package org.shop.facade.facade;

/**
 * Common interface for all facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 */
public interface Facade<RESOURCE> extends EditFacade<RESOURCE> {
	
	/**
	 * Method for creating entity.
	 * @param resource
	 * @return Entity resource.
	 */
	RESOURCE createResource(RESOURCE resource);

	/**
	 * Method for removing entity.
	 * @param id id
	 */
	void removeResource(Long id);
	
}
