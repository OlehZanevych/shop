package org.shop.facade.facade;

import org.shop.converter.AbstractResourceConverter;
import org.shop.converter.BaseConverter;
import org.shop.model.ModelWithSimpleId;
import org.shop.resource.APIResource;
import org.shop.service.EditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of Get and Update facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <SERVICE> Service class.
 * @param <ENTITYCONVERTER> Entity converter class.
 * @param <RESOURCECONVERTER> Resource converter class.
 */
@Transactional
public class DefaultEditFacade<ENTITY extends ModelWithSimpleId, RESOURCE extends APIResource,
		SERVICE extends EditService<ENTITY>, ENTITYCONVERTER extends BaseConverter<ENTITY, RESOURCE>,
		RESOURCECONVERTER extends AbstractResourceConverter<RESOURCE, ENTITY>>
		extends DefaultReadFacade<ENTITY, RESOURCE, SERVICE, ENTITYCONVERTER>
		implements EditFacade<RESOURCE> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEditFacade.class);
	
	protected RESOURCECONVERTER resourceConverter;
	
	@Override
	public void updateResource(final Long id, final RESOURCE resource) {
		LOGGER.info("Updating resource: {0} with id: {1}", resource, id);
		
		ENTITY entity = service.getPersistentEntity(id);
		
		resourceConverter.convert(resource, entity);
		
		service.updateEntity(entity);
	}

	public RESOURCECONVERTER getResourceConverter() {
		return resourceConverter;
	}

	@Required
	public void setResourceConverter(final RESOURCECONVERTER resourceConverter) {
		this.resourceConverter = resourceConverter;
	}

}
