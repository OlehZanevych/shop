package org.shop.facade.facade.session;

import org.shop.resource.session.SessionResource;

/**
 * Interface for declaring session functionality.
 * @author OlehZanevych
 *
 */
public interface SessionFacade {

	/**
	 * Method for getting current session.
	 * @return current session.
	 */
	SessionResource getCurrentSession();

}
