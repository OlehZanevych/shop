package org.shop.facade.dictionary;

import java.util.LinkedHashMap;
import java.util.List;

import org.shop.model.dictionary.Value;
import org.shop.resource.dictionary.DictionaryEntry;

/**
 * Interface for dictionary facades.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public interface DictionaryFacade<KEY extends Enum<?>> {

	/**
	 * Method for getting entry from dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry for key
	 */
	DictionaryEntry<KEY> getEntry(KEY key);
	
	/**
	 * Method for getting dictionary map.
	 * @return dictionary map
	 */
	LinkedHashMap<KEY, Value> getMap();
	
	/**
	 * Method for getting all list of entries.
	 * @return list of entries
	 */
	List<DictionaryEntry<KEY>> getList();

}
