package org.shop.dao.result.cart;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.shop.dao.annotation.CriteriaResult;
import org.shop.dao.result.AbstractCriteriaResult;
import org.shop.dao.result.transformer.ResultTransformers;
import org.shop.model.enumtype.category.Category;
import org.shop.model.cart.Cart;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Carts from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("cartCriteriaResult")
public class CartCriteriaResult extends AbstractCriteriaResult<Cart> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("productId")
			.add("count")
			.add("userId")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(IGNORED_PROJECTION_PROCESSORS)
			.put("productId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("product.id"), "product.id");
		    	}
		    })
			.put("count", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("count"), "count");
		    	}
		    })
			.put("userId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("user.id"), "user.id");
		    	}
		    })
		    .put("productTypeId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		projections.add(Projections.property("productAlias.productType.id"),
		    				"product.productType.id");
		    	}
		    })
		    .put("productTypeName", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		createAlias("productAlias.productType", "productTypeAlias", criteria, aliases);
		    		projections.add(Projections.property("productTypeAlias.name"),
		    				"product.productType.name");
		    	}
		    })
		    .put("category", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		projections.add(Projections.property("productAlias.category"), "product.category");
		    	}
		    })
		    .put("producerId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		projections.add(Projections.property("productAlias.producer.id"), "product.producer.id");
		    	}
		    })
		    .put("producerName", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		createAlias("productAlias.producer", "producerAlias", criteria, aliases);
		    		projections.add(Projections.property("producerAlias.name"), "product.producer.name");
		    	}
		    })
		    .put("cost", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		projections.add(Projections.property("productAlias.cost"), "product.cost");
		    	}
		    })
		    .put("imageUri", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		projections.add(Projections.property("productAlias.imageUri"), "product.imageUri");
		    	}
		    })
		    .put("info", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		projections.add(Projections.property("productAlias.info"), "product.info");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.put("productId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("product.id", value, Long.class));
		    	}
		    })
			.put("count", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("count", value, Short.class));
		    	}
		    })
			.put("userId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("user.id", value, Long.class));
		    	}
		    })
			.put("productTypeId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		criteria.add(createNumberRestrictions("productAlias.productType.id", value, Long.class));
		    	}
		    })
			.put("productTypeName", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		createAlias("productAlias.productType", "productTypeAlias", criteria, aliases);
		    		criteria.add(createLikeRestrictions("productTypeAlias.name", value));
		    	}
		    })
			.put("category", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		criteria.add(createEnumRestriction("productAlias.category", value, Category.class));
		    	}
		    })
			.put("producerId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		criteria.add(createNumberRestrictions("productAlias.producer.id", value, Long.class));
		    	}
		    })
			.put("producerName", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		createAlias("productAlias.producer", "producerAlias", criteria, aliases);
		    		criteria.add(createLikeRestrictions("producerAlias.name", value));
		    	}
		    })
			.put("cost", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		criteria.add(createNumberRestrictions("productAlias.cost", value, Integer.class));
		    	}
		    })
			.put("imageUri", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		criteria.add(createLikeRestrictions("productAlias.imageUri", value));
		    	}
		    })
			.put("info", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("product", "productAlias", criteria, aliases);
		    		criteria.add(createLikeRestrictions("productAlias.info", value));
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(COMMON_ORDER_PROCESSORS)
			.put("productId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "product.id";
		    	}
		    })
			.put("count", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "count";
		    	}
		    })
			.put("userId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "user.id";
		    	}
		    })
			.put("productTypeId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		return "productAlias.productType.id";
		    	}
		    })
			.put("productTypeName", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		createAlias("productAlias.productType", "productTypeAlias", criteria, aliases);
		    		return "productTypeAlias.name";
		    	}
		    })
			.put("category", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		return "productAlias.category";
		    	}
		    })
			.put("producerId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		return "productAlias.producer.id";
		    	}
		    })
			.put("producerName", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		createAlias("productAlias.producer", "producerAlias", criteria, aliases);
		    		return "producerAlias.name";
		    	}
		    })
			.put("cost", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		return "productAlias.cost";
		    	}
		    })
			.put("imageUri", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		return "productAlias.imageUri";
		    	}
		    })
			.put("info", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("product", "productAlias", criteria, aliases);
		    		return "productAlias.info";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.CART_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
