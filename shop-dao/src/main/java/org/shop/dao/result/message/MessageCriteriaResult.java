package org.shop.dao.result.message;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.shop.dao.annotation.CriteriaResult;
import org.shop.dao.result.AbstractCriteriaResult;
import org.shop.dao.result.transformer.ResultTransformers;
import org.shop.model.message.Message;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Messages from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("messageCriteriaResult")
public class MessageCriteriaResult extends AbstractCriteriaResult<Message> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("senderName")
			.add("senderEmail")
			.add("subject")
			.add("description")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(IGNORED_PROJECTION_PROCESSORS)
		    .put("senderName", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("senderName"), "senderName");
		    	}
		    })
		    .put("senderEmail", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("senderEmail"), "senderEmail");
		    	}
		    })
		    .put("subject", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("subject"), "subject");
		    	}
		    })
		    .put("description", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("description"), "description");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.put("senderName", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("senderName", value));
		    	}
		    })
			.put("senderEmail", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("senderEmail", value));
		    	}
		    })
			.put("subject", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("subject", value));
		    	}
		    })
			.put("description", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("description", value));
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(COMMON_ORDER_PROCESSORS)
			.put("senderName", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "senderName";
		    	}
		    })
			.put("senderEmail", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "senderEmail";
		    	}
		    })
			.put("subject", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "subject";
		    	}
		    })
			.put("description", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "description";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.MESSAGE_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
