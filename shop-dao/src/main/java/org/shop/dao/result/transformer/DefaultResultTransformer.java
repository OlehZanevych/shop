package org.shop.dao.result.transformer;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.hibernate.transform.AliasedTupleSubsetResultTransformer;
import org.shop.model.property.Property;
import org.springframework.beans.BeanUtils;

/**
 * Default Hibernate Criteria result transformer.
 * @author OlehZanevych
 */
public class DefaultResultTransformer extends AliasedTupleSubsetResultTransformer {

	private static final long serialVersionUID = -8047276133980128266L;
	
	private Class<?> resultClass;

	/**
	 * Constructor with result class.
	 * @param resultClass result class
	 */
	public DefaultResultTransformer(final Class<?> resultClass) {
		this.resultClass = resultClass;
	}

	@Override
	public boolean isTransformedValueATupleElement(final String[] aliases, final int tupleLength) {
		return false;
	}
	
	@Override
	public Object transformTuple(final Object[] tuple, final String[] aliases) {
		List<Property> properties = new LinkedList<Property>();
		for (int i = 0; i < tuple.length; ++i) {
			properties.add(new Property(aliases[i], tuple[i]));
		}
		return createObject(resultClass, properties);
	}
	
	/**
	 * Method for creating object add and setting its properties.
	 * @param clazz class
	 * @param properties properties
	 * @return created object
	 */
	protected Object createObject(final Class<?> clazz, final List<Property> properties) {
		Object object = BeanUtils.instantiateClass(clazz);
		Map<String, List<Property>> childPropertiesMap = new HashMap<String, List<Property>>();
		for (Property property : properties) {
			String propertyName = property.getName();
			if (propertyName.contains(".")) {
				String[] parts = propertyName.split("\\.", 2);
				propertyName = parts[0];
				property.setName(parts[1]);
				List<Property> childProperties = childPropertiesMap.get(propertyName);
				if (childProperties == null) {
					childProperties = new LinkedList<Property>();
					childPropertiesMap.put(propertyName, childProperties);
				}
				childProperties.add(property);
			} else {
				writeField(object, propertyName, property.getValue());
			}
		}
		for (Entry<String, List<Property>> childPropertiesEntry : childPropertiesMap.entrySet()) {
			String propertyName = childPropertiesEntry.getKey();
			Class<?> childClass = FieldUtils.getField(clazz, propertyName, true).getType();
			Object propertyValue = createObject(childClass, childPropertiesEntry.getValue());
			writeField(object, propertyName, propertyValue);
		}
		return object;
	}
	
	/**
	 * Method for setting object field.
	 * @param object object
	 * @param fieldName field name
	 * @param fieldValue field value
	 */
	protected void writeField(final Object object, final String fieldName, final Object fieldValue) {
		try {
			FieldUtils.writeField(object,  fieldName, fieldValue, true);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(MessageFormat.format(
					"Can't save property {0} for class {1}. Please, contact dev team for fixing this issue",
					fieldName, object.getClass()));
		}
	}
	
}
