package org.shop.dao.result.product;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.shop.dao.annotation.CriteriaResult;
import org.shop.dao.result.AbstractCriteriaResult;
import org.shop.dao.result.transformer.ResultTransformers;
import org.shop.model.enumtype.category.Category;
import org.shop.model.product.Product;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Products from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("productCriteriaResult")
public class ProductCriteriaResult extends AbstractCriteriaResult<Product> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("productTypeId")
			.add("category")
			.add("producerId")
			.add("cost")
			.add("imageUri")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(IGNORED_PROJECTION_PROCESSORS)
		    .put("productTypeId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("productType.id"), "productType.id");
		    	}
		    })
		    .put("category", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("category"), "category");
		    	}
		    })
		    .put("producerId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("producer.id"), "producer.id");
		    	}
		    })
		    .put("cost", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("cost"), "cost");
		    	}
		    })
		    .put("imageUri", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("imageUri"), "imageUri");
		    	}
		    })
		    .put("info", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    		projections.add(Projections.property("info"), "info");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.put("productTypeId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("productType.id", value, Long.class));
		    	}
		    })
			.put("category", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createEnumRestriction("category", value, Category.class));
		    	}
		    })
			.put("producerId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("producer.id", value, Long.class));
		    	}
		    })
			.put("cost", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("cost", value, Integer.class));
		    	}
		    })
			.put("imageUri", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("imageUri", value));
		    	}
		    })
			.put("info", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("info", value));
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(COMMON_ORDER_PROCESSORS)
			.put("productTypeId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "productType.id";
		    	}
		    })
			.put("category", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "category";
		    	}
		    })
			.put("producerId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "producer.id";
		    	}
		    })
			.put("cost", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "cost";
		    	}
		    })
			.put("imageUri", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "imageUri";
		    	}
		    })
			.put("info", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "info";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.PRODUCT_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
