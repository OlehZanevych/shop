package org.shop.dao.result;

import java.util.List;

import org.hibernate.Session;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.pagination.PagedResult;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.Request;

/**
 * Interface, that is used for receiving
 * data using Hibernate Session.
 * @author OlehZanevych
 * 
 * @param <ENTITY> entity class
 */
public interface CriteriaResult<ENTITY extends ModelWithSimpleId> {
	
	/**
	 * Method for getting entity by id.
	 * @param id identifier
	 * @param session Hibernate session
	 * @param entityClass entity class
	 * @return entity
	 */
	ENTITY getEntityById(Long id, Session session, Class<ENTITY> entityClass);
	
	/**
	 * Method for getting entity by id and attached request.
	 * @param id identifier
	 * @param request request
	 * @param session Hibernate session
	 * @param entityClass entity class
	 * @return entity
	 */
	ENTITY getEntityById(Long id, Request request, Session session, Class<ENTITY> entityClass);
	
	/**
	 * Method for getting paged entities for request.
	 * @param request PagedRequest
	 * @param session Hibernate Session
	 * @param entityClass entity class
	 * @param userId current userId
	 * @return PagedResult
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request, Session session, Class<ENTITY> entityClass, Long userId);
	
	/**
	 * Method for getting entities list for request.
	 * @param request PagedRequest
	 * @param session Hibernate Session
	 * @param entityClass entity class
	 * @param userId current userId
	 * @return entities list
	 */
	List<ENTITY> getEntitiesList(PagedRequest request, Session session, Class<ENTITY> entityClass, Long userId);
	
}
