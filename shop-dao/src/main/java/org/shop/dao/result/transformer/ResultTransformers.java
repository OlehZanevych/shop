package org.shop.dao.result.transformer;

import org.hibernate.transform.ResultTransformer;
import org.shop.model.cart.Cart;
import org.shop.model.message.Message;
import org.shop.model.producer.Producer;
import org.shop.model.product.Product;
import org.shop.model.product.type.ProductType;
import org.shop.model.user.User;

/**
 * Class that contains all Hibernate Criteria result transformers.
 * @author OlehZanevych
 */
public class ResultTransformers {
	
	public static final ResultTransformer CART_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Cart.class);
	
	public static final ResultTransformer MESSAGE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Message.class);
	
	public static final ResultTransformer PRODUCER_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Producer.class);
	
	public static final ResultTransformer PRODUCT_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Product.class);
	
	public static final ResultTransformer PRODUCT_TYPE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(ProductType.class);
	
	public static final ResultTransformer USER_RESULT_TRANSFORMER =
			new DefaultResultTransformer(User.class);

}
