package org.shop.dao.result;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;
import org.shop.dao.exception.EntityNotFoundException;
import org.shop.dao.numberparser.NumberParser;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.pagination.PagedResult;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains common methods for receiving
 * data from Hibernate Criteria.
 * @author OlehZanevych
 * 
 * @param <ENTITY> entity class
 */
public abstract class AbstractCriteriaResult<ENTITY extends ModelWithSimpleId>
		implements CriteriaResult<ENTITY> {
	
	private static final String RESTRICTIONS_SEPARATOR = ";";
	
	private static final String CASES_SEPARATOR = ",";
	
	private static final String NULL = "null";
	
	private static final String NOTNULL = "notnull";
	
	private static final String EMPTY = "empty";
	
	private static final String NOTEMPTY = "notempty";
	
	private static final String ALL = "all";
	
	private static final String USER_ID = "userId";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCriteriaResult.class);
	
	protected interface ProjectionProcessor {
		/**
		 * Method for adding projection.
		 * @param criteria Hibernate Criteria
		 * @param projections projections
		 * @param aliases set of aliases
		 */
		void addProjection(Criteria criteria, ProjectionList projections, Set<String> aliases);
	}
	
	protected interface ParameterProcessor {
		/**
		 * Method for creation restriction for current value.
		 * @param value value
		 * @param criteria Hibernate Criteria
		 * @param aliases set of aliases
		 * @param userId current userId
		 */
		void processParameter(String value, Criteria criteria, Set<String> aliases, Long userId);
	}
	
	protected interface OrderProcessor {
		/**
		 * Method for getting order field.
		 * @param criteria Hibernate Criteria
		 * @param aliases set of aliases
		 * @return order field
		 */
		String getOrderField(Criteria criteria, Set<String> aliases);
	}
	
	protected static final ImmutableMap<String, ProjectionProcessor> IGNORED_PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
		    .put("id", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases) {
		    		
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, ParameterProcessor> COMMON_PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
		    .put("id", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("id", value, Long.class));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> COMMON_ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
		    .put("id", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "id";
		    	}
		    })
		    .build();
	
	@Override
	public ENTITY getEntityById(final Long id, final Session session, final Class<ENTITY> entityClass) {
		
		LOGGER.info("Getting {0} entity with id: {1}", entityClass.getSimpleName(), id);
		
		Criteria criteria = session.createCriteria(entityClass).add(Restrictions.eq("id", id));
		
		Set<String> aliases = new HashSet<String>();
		
		ProjectionList projections = Projections.projectionList().add(Projections.property("id"), "id");
		addAllProjections(criteria, projections, aliases);
		
		return getEntity(criteria, projections, id, entityClass);
	}
	
	@Override
	public ENTITY getEntityById(final Long id, final Request request, final Session session,
			final Class<ENTITY> entityClass) {
		
		LOGGER.info("Getting {0} entity with id: {1} and attached request: {2}",
				entityClass.getSimpleName(), id, request);
		
		Criteria criteria = session.createCriteria(entityClass).add(Restrictions.eq("id", id));
		
		Set<String> aliases = new HashSet<String>();
		
		ProjectionList projections = Projections.projectionList().add(Projections.property("id"), "id");
		String[] fields = request.getFields();
		if (fields == null) {
			addAllProjections(criteria, projections, aliases);
		} else {
			addProjections(fields, criteria, projections, aliases);
		}
		
		return getEntity(criteria, projections, id, entityClass);
	}
	
	@Override
	public PagedResult<ENTITY> getEntities(final PagedRequest request, final Session session, final Class<ENTITY> entityClass,
			final Long userId) {
		
		LOGGER.info("Getting paged entities for request: {1}", request);
		
		Criteria criteria = session.createCriteria(entityClass);
		
		ResultTransformer resultTransformer = getResultTransformer();
		
		Set<String> aliases = new HashSet<String>();
		
		processParameters(request.getParameters(), criteria, aliases, userId);
		
		Long count = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		
		String[] fields = request.getFields();
		if (fields != null && fields.length == 1 && fields[0].equals("false")) {
			return new PagedResult<ENTITY>(count);
		} else {
			ProjectionList projections = Projections.projectionList().add(Projections.property("id"), "id");
			if (fields != null && fields.length == 1 && fields[0].equals(ALL)) {
				addAllProjections(criteria, projections, aliases);
			} else {
				if (fields == null) {
					List<String> fieldsList = getDefaultFields();
					fields = fieldsList.toArray(new String[fieldsList.size()]);
				}
				addProjections(fields, criteria, projections, aliases);
			}
			criteria.setProjection(projections).setResultTransformer(resultTransformer);
			
			Integer offset = request.getOffset();
			if (offset != null) {
				criteria.setFirstResult(offset);
			}
			
			Integer limit = request.getLimit();
			if (limit != null) {
				criteria.setMaxResults(limit);
			}
			
			addOrders(request.getOrders(), criteria, aliases);
			
			List<ENTITY> entities = criteria.list();
			
			return new PagedResult<ENTITY>(offset, limit, count, entities);
		}
	}
	
	@Override
	public List<ENTITY> getEntitiesList(final PagedRequest request, final Session session,
			final Class<ENTITY> entityClass, final Long userId) {
		
		LOGGER.info("Getting entities list for request: {1}", request);
		
		Criteria criteria = session.createCriteria(entityClass);
		
		ResultTransformer resultTransformer = getResultTransformer();
		
		Set<String> aliases = new HashSet<String>();
		
		processParameters(request.getParameters(), criteria, aliases, userId);
		
		ProjectionList projections = Projections.projectionList().add(Projections.property("id"), "id");
		
		String[] fields = request.getFields();
		if (fields != null && fields.length == 1 && fields[0].equals(ALL)) {
			addAllProjections(criteria, projections, aliases);
		} else {
			if (fields == null) {
				List<String> fieldsList = getDefaultFields();
				fields = fieldsList.toArray(new String[fieldsList.size()]);
			}
			addProjections(fields, criteria, projections, aliases);
		}
		criteria.setProjection(projections).setResultTransformer(resultTransformer);
		
		Integer offset = request.getOffset();
		if (offset != null) {
			criteria.setFirstResult(offset);
		}
		
		Integer limit = request.getLimit();
		if (limit != null) {
			criteria.setMaxResults(limit);
		}
		
		addOrders(request.getOrders(), criteria, aliases);
		
		return criteria.list();
	}
	
	/**
	 * Method for processing filtering parameters.
	 * @param parameters parameters
	 * @param criteria Hibernate criteria
	 * @param aliases aliases
	 * @param userId current userId
	 */
	protected void processParameters(final Map<String, String> parameters, final Criteria criteria, final Set<String> aliases,
			final Long userId) {
		
		if (parameters != null && !parameters.isEmpty()) {
			Map<String, ParameterProcessor> parameterProcessors = getParameterProcessors();
			for (Entry<String, String> parameter : parameters.entrySet()) {
				String parameterName = parameter.getKey();
				String parameterValue = parameter.getValue();
				ParameterProcessor parameterProcessor = parameterProcessors.get(parameterName);
				if (parameterProcessor == null) {
					throw new IllegalArgumentException(MessageFormat.format("Parameter {0} is not allowed", parameterName));
				}
				parameterProcessor.processParameter(parameterValue, criteria, aliases, userId);
			}
		}
	}
	
	/**
	 * Method for adding all projections.
	 * @param criteria Hibernate criteria
	 * @param projections projection list
	 * @param aliases aliases
	 */
	protected void addAllProjections(final Criteria criteria, final ProjectionList projections,
			final Set<String> aliases) {
		
		for (ProjectionProcessor projectionProcessor : getProjectionProcessors().values()) {
			projectionProcessor.addProjection(criteria, projections, aliases);
		}
	}
	
	/**
	 * Method for adding projections for fields.
	 * @param fields fields
	 * @param criteria Hibernate criteria
	 * @param projections projection list
	 * @param aliases aliases
	 */
	protected void addProjections(final String[] fields, final Criteria criteria, final ProjectionList projections,
			final Set<String> aliases) {
		
		Map<String, ProjectionProcessor> projectionProcessors = getProjectionProcessors();
		for (String field : fields) {
			ProjectionProcessor projectionProcessor = projectionProcessors.get(field);
			if (projectionProcessor != null) {
				projectionProcessor.addProjection(criteria, projections, aliases);
			} else {
				throw new IllegalArgumentException(MessageFormat.format("Projection of {0} is not supported", field));
			}
		}
	}
	
	/**
	 * Method for adding orders.
	 * @param orders orders
	 * @param criteria Hibernate criteria
	 * @param aliases aliases
	 */
	protected void addOrders(final String[] orders, final Criteria criteria, final Set<String> aliases) {
		if (orders != null) {
			Map<String, OrderProcessor> orderProcessors = getOrderProcessors();
			for (String order : orders) {
				String[] orderParts = order.split("-");
				OrderProcessor orderProcessor = orderProcessors.get(orderParts[0]);
				if (orderProcessor == null) {
					throw new IllegalArgumentException(MessageFormat.format("Ordering by {0} is not allowed", orderParts[0]));
				}
				if (orderParts.length == 2 && orderParts[1].equals("desc")) {
					criteria.addOrder(Order.desc(orderProcessor.getOrderField(criteria, aliases)));
				} else {
					criteria.addOrder(Order.asc(orderProcessor.getOrderField(criteria, aliases)));
				}
			}
		}
	}
	
	/**
	 * Method for getting entity.
	 * @param criteria Hibernate criteria
	 * @param projections projection list
	 * @param id identifier
	 * @param entityClass entity class
	 * @return entity
	 */
	protected ENTITY getEntity(final Criteria criteria, final ProjectionList projections,
			final Long id, final Class<ENTITY> entityClass) {
		
		criteria.setProjection(projections).setResultTransformer(getResultTransformer());
		
		ENTITY entity = (ENTITY) criteria.uniqueResult();
		if (entity == null) {
        	LOGGER.error("Entity {0} with id {1} doesn't exist", entityClass, id);
        	throw new EntityNotFoundException("Entity doesn't exist", entityClass, id);
        }
		
		return entity;
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all like restrictions.
	 * @param field field
	 * @param requirements requirements
	 * @return restriction
	 */
	protected static Criterion createLikeRestrictions(final String field, final String requirements) {
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createLikeRestriction(field, i)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all User identifier restrictions.
	 * @param field field
	 * @param requirements requirements
	 * @param userId current User id
	 * @return restriction
	 */
	protected static Criterion createUserIdRestrictions(final String field, final String requirements,
			final Long userId) {
		
		String processedRequirements = requirements.replace(USER_ID,
				userId != null ? userId.toString() : "null");
		return createNumberRestrictions(field, processedRequirements, Long.class);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all numerical restrictions.
	 * @param field field
	 * @param requirements requirements
	 * @param numberClass number class
	 * @return restriction
	 */
	protected static Criterion createNumberRestrictions(final String field, final String requirements,
			final Class<? extends Number> numberClass) {
		
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createNumberRestriction(field, i, numberClass)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	
	/**
	 * Method for creating restriction for match collection size
	 * with all restrictions.
	 * @param collectionField collection field
	 * @param requirements requirements
	 * @return restriction
	 */
	protected static Criterion createSizeRestrictions(final String collectionField, final String requirements) {
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createSizeRestriction(collectionField, i)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	/**
	 * Method for creating restriction for like match current field
	 * with one of variants.
	 * @param field field
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createLikeRestriction(final String field, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					switch (i) {
						case NULL:
							return Restrictions.isNull(field);
						case NOTNULL:
							return Restrictions.isNotNull(field);
						default:
							return Restrictions.ilike(field, i, MatchMode.ANYWHERE);
					}
				})
				.toArray(Criterion[]::new);
		return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of parsed numerical range variants.
	 * @param field field
	 * @param variants variants
	 * @param numberClass number class
	 * @return restriction
	 */
	protected static Criterion createNumberRestriction(final String field, final String variants,
			final Class<? extends Number> numberClass) {
		
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					int length = i.length();
					if (length > 2) {
						switch (i.substring(0, 2)) {
				            case "<=":
				            	return Restrictions.le(field,
				            			NumberParser.parse(i.substring(2), numberClass));
				            case ">=":
				            	return Restrictions.ge(field,
				            			NumberParser.parse(i.substring(2), numberClass));
				            case "!=":
				            	return Restrictions.ne(field,
				            			NumberParser.parse(i.substring(2), numberClass));
						}
					}
					if (length > 1) {
						switch (i.substring(0, 1)) {
				            case "<":
				            	return Restrictions.lt(field,
				            			NumberParser.parse(i.substring(1), numberClass));
				            case ">":
				            	return Restrictions.gt(field,
				            			NumberParser.parse(i.substring(1), numberClass));
						}
					}
					switch (i) {
						case NULL:
							return Restrictions.isNull(field);
						case NOTNULL:
							return Restrictions.isNotNull(field);
						default:
							return Restrictions.eq(field, NumberParser.parse(i, numberClass));
					}
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match collection size
	 * with one of parsed range variants.
	 * @param collectionField collection field
	 * @param variants size variants
	 * @return restriction
	 */
	protected static Criterion createSizeRestriction(final String collectionField, final String variants) {
		
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					int length = i.length();
					if (length > 2) {
						switch (i.substring(0, 2)) {
				            case "<=":
				            	return Restrictions.sizeLe(collectionField, Integer.parseInt(i.substring(2)));
				            case ">=":
				            	return Restrictions.sizeGe(collectionField, Integer.parseInt(i.substring(2)));
				            case "!=":
				            	return Restrictions.sizeNe(collectionField, Integer.parseInt(i.substring(2)));
						}
					}
					if (length > 1) {
						switch (i.substring(0, 1)) {
				            case "<":
				            	return Restrictions.sizeLt(collectionField, Integer.parseInt(i.substring(1)));
				            case ">":
				            	return Restrictions.sizeGt(collectionField, Integer.parseInt(i.substring(1)));
						}
					}
					switch (i) {
						case EMPTY:
							return Restrictions.isEmpty(collectionField);
						case NOTEMPTY:
							return Restrictions.isNotEmpty(collectionField);
						default:
							return Restrictions.sizeEq(collectionField, Integer.parseInt(i));
					}
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of parsed enum variants.
	 * @param field field
	 * @param variants variants
	 * @param enumType enum type
	 * @return restriction
	 */
	protected static Criterion createEnumRestriction(final String field, final String variants,
			final Class<? extends Enum> enumType) {
		
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					switch (i) {
						case NULL:
							return Restrictions.isNull(field);
						case NOTNULL:
							return Restrictions.isNotNull(field);
					}
					try {
						return Restrictions.eq(field, Enum.valueOf(enumType, i));
					} catch (Exception e) {
						throw new IllegalArgumentException(MessageFormat.format("Value {0} is not allowed for filtering for enum field {1}", i, field));
					}
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of parsed boolean variants.
	 * @param field field
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createBooleanRestriction(final String field, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					switch (i) {
						case NULL:
							return Restrictions.isNull(field);
						case NOTNULL:
							return Restrictions.isNotNull(field);
						default:
							return Restrictions.eq(field, Boolean.parseBoolean(i));
					}
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of polygon restrictions.
	 * @param field field
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createPolygonRestriction(final String field, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					switch (i) {
						case NULL:
							return Restrictions.isNull(field);
						default:
							return Restrictions.isNotNull(field);
					}
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating alias for current field.
	 * @param field field
	 * @param alias alias
	 * @param criteria Hibernate Criteria
	 * @param aliases set of aliases
	 */
	protected static void createAlias(final String field, final String alias, final Criteria criteria,
			final Set<String> aliases) {
		
		if (!aliases.contains(alias)) {
			criteria.createAlias(field, alias, JoinType.LEFT_OUTER_JOIN);
			aliases.add(alias);
		}
	}
	
	/**
	 * Method for getting result transformer.
	 * @return result transformer
	 */
	protected abstract ResultTransformer getResultTransformer();
	
	/**
	 * Method for getting default fields.
	 * @return default fields
	 */
	protected abstract List<String> getDefaultFields();
	
	/**
	 * Method for getting projection processors.
	 * @return projection processors
	 */
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return IGNORED_PROJECTION_PROCESSORS;
	}
	
	/**
	 * Method for getting filtering parameter processors.
	 * @return filtering parameter processors
	 */
	protected Map<String, ParameterProcessor> getParameterProcessors() {
		return COMMON_PARAMETER_PROCESSORS;
	}
	
	/**
	 * Method for getting order processors.
	 * @return order processors
	 */
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return COMMON_ORDER_PROCESSORS;
	}
}
