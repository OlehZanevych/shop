package org.shop.dao.persistence;

import org.hibernate.Session;
import org.shop.model.ModelWithSimpleId;
/**
 * Interface, that is used to encapsulate all work
 * with entity manager.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity Class
 */
public interface PersistenceManager<ENTITY extends ModelWithSimpleId> {

	/**
	 * Method for creating entity.
	 * @param entity Instance of entity
	 * @return Entity with Id
	 */
    ENTITY create(ENTITY entity);

    /**
     * Finds entity from database by identifier.
     * @param clazz entity class
     * @param id entity identifier
     * @return Entity instance
     */
    ENTITY findById(Class<ENTITY> clazz, Long id);

    /**
     * Updates entity.
     * @param entity entity instance
     * @return Updated instance
     */
    ENTITY update(ENTITY entity);

    /**
     * Removes entity.
     * @param clazz entity class
     * @param id entity identifier
     */
    void remove(Class<ENTITY> clazz, Long id);
	
    /**
     * Get Hibernate Session.
     * @return Hibernate Session instance
     */
	Session getSession();
}
