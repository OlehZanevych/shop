package org.shop.dao.persistence;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.shop.dao.exception.EntityNotFoundException;
import org.shop.model.ModelWithSimpleId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Persistence Manager to work with entity manager.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
@Repository("persistenceManager")
public class DefaultPersistenceManager<ENTITY extends ModelWithSimpleId>
		implements PersistenceManager<ENTITY> {
	
	private static final String DELETE_SQL = "DELETE %s WHERE id = :id";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPersistenceManager.class);

	@PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public ENTITY create(final ENTITY entity) {
		LOGGER.info("Saving entity for class: {}", entity.getClass());
		entityManager.persist(entity);
		return entity;
    }

    @Override
    public ENTITY findById(final Class<ENTITY> clazz, final Long id) {
    	LOGGER.info("Get entity: {} by id: {}", clazz, id);
        ENTITY entity = entityManager.find(clazz, id);
        if (entity == null) {
        	LOGGER.error("{} entity with id {} doesn't exist", clazz.getSimpleName(), id);
        	throw new EntityNotFoundException("Entity doesn't exist", clazz, id);
        }
        return entity;
    }

    @Override
    public ENTITY update(final ENTITY entity) {
    	LOGGER.info("Updating entity for class: {}", entity.getClass());
        return entityManager.merge(entity);
    }

    @Override
    public void remove(final Class<ENTITY> clazz, final Long id) {
    	LOGGER.info("Removing {} entity with id: {}", clazz.getName(), id);
    	String deleteSQL = String.format(DELETE_SQL, clazz.getName());
    	int count = entityManager.createQuery(deleteSQL).setParameter("id", id).executeUpdate();
    	if (count == 0) {
    		LOGGER.error("{} entity with id {} doesn't exist", clazz.getSimpleName(), id);
        	throw new EntityNotFoundException("Entity doesn't exist", clazz, id);
    	}
    }

    @Override
    public Session getSession() {
    	LOGGER.info("Getting Hibernate Session");
    	return entityManager.unwrap(Session.class);
    }
	
}
