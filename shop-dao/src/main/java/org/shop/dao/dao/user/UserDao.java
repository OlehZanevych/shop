package org.shop.dao.dao.user;

import org.shop.dao.dao.Dao;
import org.shop.model.user.User;

/**
 * User dao interface.
 * @author OlehZanevych
 */
public interface UserDao extends Dao<User> {

	/**
	 * Method for getting user by login.
	 * @param login login
	 * @return User.
	 */
	User getUserByLogin(String login);
	
}
