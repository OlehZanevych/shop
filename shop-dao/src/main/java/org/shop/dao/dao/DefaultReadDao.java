package org.shop.dao.dao;

import java.util.List;

import org.shop.dao.persistence.PersistenceManager;
import org.shop.dao.result.CriteriaResult;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.pagination.PagedResult;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation, that has methods to Get entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity type.
 */
public class DefaultReadDao<ENTITY extends ModelWithSimpleId> implements ReadDao<ENTITY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultReadDao.class);
	
	protected PersistenceManager<ENTITY> persistenceManager;
	
	protected Class<ENTITY> entityClass;
	
	protected CriteriaResult<ENTITY> criteriaResult;
	
	@Override
	public ENTITY getEntityById(final Long id) {
		LOGGER.info("Getting {} entity with id: {}", entityClass.getSimpleName(), id);
		return criteriaResult.getEntityById(id, persistenceManager.getSession(), entityClass);
	}
	
	@Override
	public ENTITY getEntityById(final Long id, final Request request) {
		LOGGER.info("Getting {} entity with id: {} and attached request: {}",
				entityClass.getSimpleName(), id, request);
		return criteriaResult.getEntityById(id, request, persistenceManager.getSession(), entityClass);
	}
	
	@Override
	public PagedResult<ENTITY> getEntities(final PagedRequest request, final Long userId) {
		LOGGER.info("Getting paged result for {}", entityClass.getSimpleName());
		return criteriaResult.getEntities(request, persistenceManager.getSession(), entityClass, userId);
	}
	
	@Override
	public List<ENTITY> getEntitiesList(final PagedRequest request, final Long userId) {
		LOGGER.info("Getting entities list for {}", entityClass.getSimpleName());
		return criteriaResult.getEntitiesList(request, persistenceManager.getSession(), entityClass, userId);
	}

	public PersistenceManager<ENTITY> getPersistenceManager() {
		return persistenceManager;
	}

	public void setPersistenceManager(final PersistenceManager<ENTITY> persistenceManager) {
		this.persistenceManager = persistenceManager;
	}

	public Class<ENTITY> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(final Class<ENTITY> entityClass) {
		this.entityClass = entityClass;
	}

	public CriteriaResult<ENTITY> getCriteriaResult() {
		return criteriaResult;
	}

	public void setCriteriaResult(final CriteriaResult<ENTITY> criteriaResult) {
		this.criteriaResult = criteriaResult;
	}

}
