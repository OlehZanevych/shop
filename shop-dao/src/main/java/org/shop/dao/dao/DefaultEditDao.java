package org.shop.dao.dao;

import org.shop.model.ModelWithSimpleId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation, that has methods to Get and Update entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity type.
 */
public class DefaultEditDao<ENTITY extends ModelWithSimpleId>
		extends DefaultReadDao<ENTITY> implements EditDao<ENTITY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEditDao.class);
	
	@Override
	public ENTITY getPersistentEntityById(final Long id) {
		LOGGER.info("Getting {} persistent entity with id: {}", entityClass.getSimpleName(), id);
		return persistenceManager.findById(entityClass, id);
	}
	
	@Override
	public void update(final ENTITY entity) {
		LOGGER.info("Updating {} entity with id: {}, {}", entityClass.getSimpleName(), entity.getId(), entity);
		persistenceManager.update(entity);
	}
	
}
