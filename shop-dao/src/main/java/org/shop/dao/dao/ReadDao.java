package org.shop.dao.dao;

import java.util.List;

import org.shop.model.pagination.PagedResult;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.Request;

/**
 * Interface, that has methods to Get entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
public interface ReadDao<ENTITY> {
	
	/**
	 * Method for getting entity by id.
	 * @param id identifier
	 * @return entity
	 */
	ENTITY getEntityById(Long id);
	
	/**
	 * Method for finding entity by id with attached request.
	 * @param id identifier
	 * @param request attached request
	 * @return Entity
	 */
	ENTITY getEntityById(Long id, Request request);
	
	/**
	 * Method for getting paged result.
	 * @param request paged request
	 * @param userId current User id
	 * @return paged result.
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request, Long userId);
	
	/**
	 * Method for getting entities list.
	 * @param request paged request
	 * @param userId current User id
	 * @return paged result.
	 */
	List<ENTITY> getEntitiesList(PagedRequest request, Long userId);

}
