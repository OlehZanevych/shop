package org.shop.dao.dao;

/**
 * Interface, that has methods to Get and Update entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
public interface EditDao<ENTITY> extends ReadDao<ENTITY> {
	
	/**
	 * Getting persistent entity by id.
	 * @param id identifier
	 * @return persistent entity
	 */
	ENTITY getPersistentEntityById(Long id);
	
	/**
	 * Updating entity.
	 * @param entity entity
	 */
	void update(ENTITY entity);
}
