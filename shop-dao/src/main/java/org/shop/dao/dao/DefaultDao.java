package org.shop.dao.dao;

import org.shop.model.ModelWithSimpleId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default implementation, that has all methods to work with entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity type.
 */
public class DefaultDao<ENTITY extends ModelWithSimpleId> extends DefaultEditDao<ENTITY>
		implements Dao<ENTITY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDao.class);

	@Override
	public void save(final ENTITY entity) {
		LOGGER.info("Saving {} entity: {}", entityClass.getSimpleName(), entity);
		persistenceManager.create(entity);
	}

	@Override
	public void delete(final Long id) {
		LOGGER.info("Removing {} entity with id: {}", entityClass.getSimpleName(), id);
		persistenceManager.remove(entityClass, id);
	}
	
}
