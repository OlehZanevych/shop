package org.shop.dao.dao;

/**
 * Interface, that has all methods to work with entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
public interface Dao<ENTITY> extends EditDao<ENTITY> {
	
	/**
	 * Method for saving entity.
	 * @param entity entity
	 */
	void save(ENTITY entity);
	
	/**
	 * Method for deleting entity.
	 * @param id identifier
	 */
	void delete(Long id);
	
}
