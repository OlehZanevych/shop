package org.shop.dao.dao.user;

import org.hibernate.criterion.Restrictions;
import org.shop.dao.dao.DefaultDao;
import org.shop.model.user.User;

/**
 * Default User Dao.
 * @author OlehZanevych
 */
public class DefaultUserDao extends DefaultDao<User> implements UserDao {
	
	@Override
	public User getUserByLogin(final String login) {
		return (User) getPersistenceManager().getSession().createCriteria(User.class)
				.add(Restrictions.eq("login", login)).uniqueResult();
	}

}
