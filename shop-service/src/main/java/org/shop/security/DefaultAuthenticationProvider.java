package org.shop.security;

import org.shop.annotation.Security;
import org.shop.model.enumtype.role.Role;
import org.shop.model.session.Session;
import org.shop.model.user.User;
import org.shop.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default Authentication Provider.
 * @author OlehZanevych
 */
@Security("defaultAuthenticationProvider")
public class DefaultAuthenticationProvider implements AuthenticationProvider {
    
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAuthenticationProvider.class);

    @Resource(name = "userService")
    private UserService userService;
    
    @Resource(name = "bCryptPasswordEncoder")
    private PasswordEncoder passwordEncoder;
    
    @Override
    public boolean supports(final Class<?> clazz) {
    	return clazz.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Override
    @Transactional
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        
    	String login = authentication.getName();
        String password = authentication.getCredentials().toString();
        
        LOG.info(MessageFormat.format("Starting to authenticate user with login: {0}", login));

        return getAuthentication(login, password);
    }

    /**
     * Method for getting authentification.
     * @param login
     * @param password
     * @return authentification.
     */
    private Authentication getAuthentication(final String login, final String password) {
        
    	User user = getUser(login, password);
    	
    	List<String> userRoles = new LinkedList<String>();
    	
    	Role role = user.getRole();
    	
    	switch (role) {
    		case BAN:
    			userRoles.add("BAN");
    			break;
    		case CUSTOMER:
    			userRoles.add("CUSTOMER");
    			break;
    		case MODERATOR:
    			userRoles.add("CUSTOMER");
    			userRoles.add("MODERATOR");
    			break;
    		case ADMIN:
    			userRoles.add("CUSTOMER");
    			userRoles.add("MODERATOR");
    			userRoles.add("ADMIN");
    			break;
    		default:
    			userRoles.add("BAN");
    	}
    	
    	
    	Collection<GrantedAuthority> authorities = userRoles.stream()
        		.map(i -> new SimpleGrantedAuthority(i)).collect(Collectors.toList());
        
        Session session = new Session(user.getId());

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password, authorities);
        token.setDetails(session);

        return token;
    }

	/**
     * Method for getting user.
     * @param login
     * @param password
     * @return user.
     */
    private User getUser(final String login, final String password) {
    	User user = null;
        try {
            user =  userService.getUserByLogin(login);
            Assert.isTrue(passwordEncoder.matches(password, user.getPasswordHash()));
        } catch (Exception e) {
            LOG.error("Can't find user for login: " + login, e);
            throw new BadCredentialsException("Bad creditials");
        }
        return user;
    }

}
