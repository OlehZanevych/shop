package org.shop.service;

/**
 * Common interface for Get and Update service methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
public interface EditService<ENTITY> extends ReadService<ENTITY> {
	
	/**
	 * Getting persistent entity.
	 * @param id id
	 * @return persistent entity
	 */
	ENTITY getPersistentEntity(Long id);

	/**
	 * Updating entity.
	 * @param entity entity
	 */
	void updateEntity(ENTITY entity);
}
