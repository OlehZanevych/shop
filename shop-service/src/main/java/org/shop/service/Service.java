package org.shop.service;

/**
 * Common interface for all service methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
public interface Service<ENTITY> extends EditService<ENTITY> {

	/**
	 * Method for creating entity.
	 * @param entity entity
	 */
	void createEntity(ENTITY entity);

	/**
	 * Method for removing entity.
	 * @param id id
	 */
	void removeEntity(Long id);

}
