package org.shop.service;

import org.shop.dao.dao.Dao;
import org.shop.model.ModelWithSimpleId;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of all service methods.
 * NOTE: If you have some custom logic for any of methods below in this class,
 * you can easily extend this class, write some custom logic.
 * 
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity, Please see shop-model module.
 * @param <DAO> Dao
 */
@Transactional
public class DefaultService<ENTITY extends ModelWithSimpleId, DAO extends Dao<ENTITY>>
	extends DefaultEditService<ENTITY, DAO> implements Service<ENTITY> {

	@Override
	public void createEntity(final ENTITY entity) {
		dao.save(entity);
	}

	@Override
	public void removeEntity(final Long id) {
		dao.delete(id);
	}
	
}
