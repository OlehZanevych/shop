package org.shop.service;

import java.util.List;

import org.shop.dao.dao.ReadDao;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.pagination.PagedResult;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.Request;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of Get service methods.
 * NOTE: If you have some custom logic for any of methods below in this class,
 * you can easily extend this class, write some custom logic.
 * 
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity, Please see shop-model module.
 * @param <DAO> Dao
 */
@Transactional
public class DefaultReadService<ENTITY extends ModelWithSimpleId,
		DAO extends ReadDao<ENTITY>> implements ReadService<ENTITY> {
	
	protected DAO dao;
	
	@Override
	public ENTITY getEntity(final Long id) {
		return dao.getEntityById(id);
	}
	
	@Override
	public ENTITY getEntity(final Long id, final Request request) {
		return dao.getEntityById(id, request);
	}
	
	@Override
	public PagedResult<ENTITY> getEntities(final PagedRequest request, final Long userId) {
		return dao.getEntities(request, userId);
	}
	
	@Override
	public List<ENTITY> getEntitiesList(final PagedRequest request, final Long userId) {
		return dao.getEntitiesList(request, userId);
	}
	
	public void setDao(final DAO defaultDao) {
		this.dao = defaultDao;
	}

	public DAO getDao() {
		return dao;
	}
	
}
