package org.shop.service;

import org.shop.dao.dao.EditDao;
import org.shop.model.ModelWithSimpleId;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of Get and Update service methods.
 * NOTE: If you have some custom logic for any of methods below in this class,
 * you can easily extend this class, write some custom logic.
 * 
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity, Please see shop-model module.
 * @param <DAO> Dao
 */
@Transactional
public class DefaultEditService<ENTITY extends ModelWithSimpleId,
		DAO extends EditDao<ENTITY>> extends DefaultReadService<ENTITY, DAO>
		implements EditService<ENTITY> {
	
	@Override
	public ENTITY getPersistentEntity(final Long id) {
		return dao.getPersistentEntityById(id);
	}

	@Override
	public void updateEntity(final ENTITY entity) {
		dao.update(entity);
	}
}
