package org.shop.service;

import java.util.List;

import org.shop.model.pagination.PagedResult;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.Request;

/**
 * Common interface for Get service methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 */
public interface ReadService<ENTITY> {
	
	/**
	 * Method for getting entity.
	 * @param id id
	 * @return entity
	 */
	ENTITY getEntity(Long id);
	
	/**
	 * Method for getting entity.
	 * @param id id
	 * @param request Request
	 * @return entity
	 */
	ENTITY getEntity(Long id, Request request);
	
	/**
	 * Method for getting paged result.
	 * @param request request
	 * @param userId current User id
	 * @return paged result
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request, Long userId);
	
	/**
	 * Method for getting entities list.
	 * @param request request
	 * @param userId current User id
	 * @return entities list
	 */
	List<ENTITY> getEntitiesList(PagedRequest request, Long userId);


}
