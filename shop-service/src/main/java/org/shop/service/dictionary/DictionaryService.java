package org.shop.service.dictionary;

import java.util.LinkedHashMap;

import org.shop.model.dictionary.Value;

/**
 * Interface for dictionary services.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public interface DictionaryService<KEY extends Enum<?>> {

	/**
	 * Method for getting value from dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return value for key
	 */
	Value getValue(KEY key);
	
	/**
	 * Method for getting all dictionary.
	 * @return dictionary
	 */
	LinkedHashMap<KEY, Value> getDictionary();
	
}
