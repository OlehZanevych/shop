package org.shop.service.user;

import org.shop.model.user.User;

/**
 * User Service interface.
 * @author OlehZanevych
 *
 */
public interface UserService {

	/**
	 * Method for getting user by login.
	 * @param login
	 * @return User
	 */
	User getUserByLogin(String login);
	
}
