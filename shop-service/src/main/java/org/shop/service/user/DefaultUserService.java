package org.shop.service.user;

import org.shop.dao.dao.user.UserDao;
import org.shop.model.user.User;
import org.shop.service.DefaultService;

/**
 * Default User Service.
 * @author OlehZanevych
 */
public class DefaultUserService extends DefaultService<User, UserDao> implements UserService {

	@Override
	public User getUserByLogin(final String login) {
		return getDao().getUserByLogin(login);
	}

}
