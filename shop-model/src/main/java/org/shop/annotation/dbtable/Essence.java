package org.shop.annotation.dbtable;

/**
 * Annotation, that is used to describe table which contains essence.
 * @author OlehZanevych
 */
public @interface Essence {

}
