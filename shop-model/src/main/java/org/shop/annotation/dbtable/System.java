package org.shop.annotation.dbtable;

/**
 * Annotation, that is used to describe system table.
 * @author OlehZanevych
 */
public @interface System {

}
