package org.shop.annotation.dbtable;

/**
 * Annotation, that is used to describe table which is forming a relationship many to many.
 * @author OlehZanevych
 */
public @interface Relationship {

}
