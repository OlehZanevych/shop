package org.shop.model.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.shop.annotation.dbtable.Essence;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.enumtype.category.Category;
import org.shop.model.producer.Producer;
import org.shop.model.product.type.ProductType;

/**
 * Entity, that describes product.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_products")
public final class Product extends ModelWithSimpleId {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "product_type_id")
	private ProductType productType;
	
	@NotNull
	@Type(type = "org.shop.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.shop.model.enumtype.category.Category"
			)
	)
	@Column(name = "category", columnDefinition = "e_category")
	@Enumerated(EnumType.STRING)
	private Category category;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "producer_id")
	private Producer producer;
	
	@NotNull
	@Column(name = "cost")
	private Integer cost;
	
	@Column(name = "image_uri")
	private String imageUri;
	
	@Column(name = "info")
	private String info;

	/**
	 * Default constructor with no parameters.
	 */
	public Product() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Product(final Long id) {
		super(id);
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(final ProductType productType) {
		this.productType = productType;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(final Category category) {
		this.category = category;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(final Producer producer) {
		this.producer = producer;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(final Integer cost) {
		this.cost = cost;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(final String imageUri) {
		this.imageUri = imageUri;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((imageUri == null) ? 0 : imageUri.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((producer == null) ? 0 : producer.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Product other = (Product) obj;
		if (category != other.category) {
			return false;
		}
		if (cost == null) {
			if (other.cost != null) {
				return false;
			}
		} else if (!cost.equals(other.cost)) {
			return false;
		}
		if (imageUri == null) {
			if (other.imageUri != null) {
				return false;
			}
		} else if (!imageUri.equals(other.imageUri)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (producer == null) {
			if (other.producer != null) {
				return false;
			}
		} else if (!producer.equals(other.producer)) {
			return false;
		}
		if (productType == null) {
			if (other.productType != null) {
				return false;
			}
		} else if (!productType.equals(other.productType)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [productType=");
		builder.append(productType);
		builder.append(", category=");
		builder.append(category);
		builder.append(", producer=");
		builder.append(producer);
		builder.append(", cost=");
		builder.append(cost);
		builder.append(", imageUri=");
		builder.append(imageUri);
		builder.append(", info=");
		builder.append(info);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
