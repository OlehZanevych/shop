package org.shop.model.product.type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.shop.annotation.dbtable.Essence;
import org.shop.model.ModelWithSimpleId;

/**
 * Entity, that describes product type.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_product_types")
public final class ProductType extends ModelWithSimpleId {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@Column(name = "name")
	private String name;
	
	@Column(name = "info")
	private String info;

	/**
	 * Default constructor with no parameters.
	 */
	public ProductType() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public ProductType(final Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProductType other = (ProductType) obj;
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductType [name=");
		builder.append(name);
		builder.append(", info=");
		builder.append(info);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
