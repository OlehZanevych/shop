package org.shop.model;

import java.io.Serializable;

/**
 * Abstract model.
 * @author OlehZanevych
 *
 */
public abstract class Model implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected Number parentId;

	public Number getParentId() {
		return parentId;
	}

	public void setParentId(final Number parentId) {
		this.parentId = parentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Model other = (Model) obj;
		if (parentId == null) {
			if (other.parentId != null) {
				return false;
			}
		} else if (!parentId.equals(other.parentId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Model [parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
