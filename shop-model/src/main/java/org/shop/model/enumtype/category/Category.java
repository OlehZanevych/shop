package org.shop.model.enumtype.category;

/**
 * Enum, that describes category.
 * @author OlehZanevych
 */
public enum Category {
	NULL,
	MAN,
	WOMAN,
	BOY,
	GIRL
}
