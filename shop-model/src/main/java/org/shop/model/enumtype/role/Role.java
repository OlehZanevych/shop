package org.shop.model.enumtype.role;

/**
 * Enum, that describes Role.
 * @author OlehZanevych
 */
public enum Role {
	NULL,
	BAN,
	CUSTOMER,
	MODERATOR,
	ADMIN
}
