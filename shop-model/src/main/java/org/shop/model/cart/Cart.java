package org.shop.model.cart;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.shop.annotation.dbtable.Essence;
import org.shop.model.ModelWithSimpleId;
import org.shop.model.product.Product;
import org.shop.model.user.User;

/**
 * Entity, that describes cart.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_carts")
public final class Cart extends ModelWithSimpleId {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@NotNull
	@Column(name = "count")
	private Short count;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	/**
	 * Default constructor with no parameters.
	 */
	public Cart() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Cart(final Long id) {
		super(id);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(final Product product) {
		this.product = product;
	}

	public Short getCount() {
		return count;
	}

	public void setCount(final Short count) {
		this.count = count;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((count == null) ? 0 : count.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cart other = (Cart) obj;
		if (count == null) {
			if (other.count != null) {
				return false;
			}
		} else if (!count.equals(other.count)) {
			return false;
		}
		if (product == null) {
			if (other.product != null) {
				return false;
			}
		} else if (!product.equals(other.product)) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cart [product=");
		builder.append(product);
		builder.append(", count=");
		builder.append(count);
		builder.append(", user=");
		builder.append(user);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
