package org.shop.web.page.controller.registration;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Registration page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/registration")
public class RegistrationPageController {
	
	/**
	 * Getting Registration page.
	 * @return Registration page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getPage(final ModelMap model) {
		model.addAttribute("pageName", "Реєстрація");
		return "registration";
	}

}
