package org.shop.web.page.controller.logout;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for User logout.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/logout")
public class LogoutPageController {
	
	/**
	 * User logout.
	 * @return main page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String logout(final HttpServletRequest request, final HttpServletResponse response) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (authentication != null) {    
	        new SecurityContextLogoutHandler().logout(request, response, authentication);
	    }
		return "redirect:/api/main";
	}

}
