package org.shop.web.page.controller.checkout;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Checkout page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/checkout")
public class CheckoutPageController {
	
	/**
	 * Getting Checkout page.
	 * @return checkout page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getPage(final ModelMap model) {
		model.addAttribute("pageName", "Оплата");
		return "checkout";
	}

}
