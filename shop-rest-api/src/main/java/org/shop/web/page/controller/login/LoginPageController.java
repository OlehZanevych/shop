package org.shop.web.page.controller.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Login page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/login")
public class LoginPageController {
	
	/**
	 * Getting Login page.
	 * @return ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView modelAndView = new ModelAndView("login");
		modelAndView.addObject("pageName", "Вхід");
		return modelAndView;
	}

}
