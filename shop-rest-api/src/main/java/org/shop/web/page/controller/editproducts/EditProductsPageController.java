package org.shop.web.page.controller.editproducts;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.product.ProductResource;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.PagedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Main page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/editproducts")
public class EditProductsPageController {
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> productFacade;
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> productTypeFacade;
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> categoryDictionaryFacade;
	
	private static final PagedRequest PRODUCT_REQUES = new PagedRequest();
	
	private static final PagedRequest PRODUCT_TYPE_REQUEST = new PagedRequest();
	
	static {
		String[] productFields = {"id", "productTypeId", "category", "cost", "imageUri"}; 
		PRODUCT_REQUES.setFields(productFields);
		
		String[] productTypeFields = {"id", "name"}; 
		PRODUCT_TYPE_REQUEST.setFields(productTypeFields);
	}
	
	/**
	 * Getting Main page.
	 * @return Main page ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView modelAndView = new ModelAndView("editproducts");
		modelAndView.addObject("pageName", "Редагування продукції");
		modelAndView.addObject("productsList", productFacade.getResourcesList(PRODUCT_REQUES));
		modelAndView.addObject("productTypesMap", productTypeFacade.getResourcesMap(PRODUCT_TYPE_REQUEST));
		modelAndView.addObject("categoriesMap", categoryDictionaryFacade.getMap());
		return modelAndView;
	}

}
