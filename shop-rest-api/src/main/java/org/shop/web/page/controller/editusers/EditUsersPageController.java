package org.shop.web.page.controller.editusers;

import javax.annotation.Resource;

import org.shop.facade.facade.Facade;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.user.UserResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Main page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/editusers")
public class EditUsersPageController {
	
	@Resource(name = "userFacade")
	private Facade<UserResource> userFacade;
	
	private static final PagedRequest USER_REQUEST = new PagedRequest();
	
	static {
		String[] userFields = {"id", "login", "name", "email"}; 
		USER_REQUEST.setFields(userFields);
	}
	
	/**
	 * Getting Main page.
	 * @return Main page ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView modelAndView = new ModelAndView("editusers");
		modelAndView.addObject("pageName", "Редагування користувачів");
		modelAndView.addObject("usersList", userFacade.getResourcesList(USER_REQUEST));
		return modelAndView;
	}

}
