package org.shop.web.page.controller.edituser;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.role.Role;
import org.shop.resource.user.UserResource;
import org.shop.resource.request.PagedRequest;
import org.shop.web.rest.constant.RequestConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * EditUser page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/edituser")
public class EditUserPageController {
	
	@Resource(name = "userFacade")
	private Facade<UserResource> userFacade;
	
	@Resource(name = "roleDictionaryFacade")
	private DictionaryFacade<Role> roleDictionaryFacade;
	
	private static final PagedRequest USER_REQUEST = new PagedRequest();
	
	static {
		String[] userFields = {"id", "login", "surname", "firstName", "middleName",
				"email", "phone", "info", "role"}; 
		USER_REQUEST.setFields(userFields);
	}
	
	/**
	 * Getting EditUser page.
	 * @return ModelAndView
	 */
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public ModelAndView getPage(@PathVariable("id") final Long id) {
		ModelAndView modelAndView = new ModelAndView("edituser");
		modelAndView.addObject("pageName", "Редагування користувача");
		
		UserResource userResource = userFacade.getResource(id, USER_REQUEST);
		modelAndView.addObject("user", userResource);
		
		modelAndView.addObject("rolesList", roleDictionaryFacade.getList());
		
		return modelAndView;
	}

}
