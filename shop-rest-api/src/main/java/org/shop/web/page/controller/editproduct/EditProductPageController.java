package org.shop.web.page.controller.editproduct;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.producer.ProducerResource;
import org.shop.resource.product.ProductResource;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.PagedRequest;
import org.shop.web.rest.constant.RequestConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * EditProduct page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/editproduct")
public class EditProductPageController {
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> productFacade;
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> productTypeFacade;
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> categoryDictionaryFacade;
	
	@Resource(name = "producerFacade")
	private Facade<ProducerResource> producerFacade;
	
	private static final PagedRequest PRODUCT_REQUEST = new PagedRequest();
	
	private static final PagedRequest PRODUCT_TYPE_REQUEST = new PagedRequest();
	
	private static final PagedRequest PRODUCER_REQUEST = new PagedRequest();
	
	static {
		String[] productFields = {"id", "productTypeId", "category", "producerId", "cost",
				"imageUri", "info"}; 
		PRODUCT_REQUEST.setFields(productFields);
		
		String[] productTypeFields = {"id", "name"}; 
		PRODUCT_TYPE_REQUEST.setFields(productTypeFields);
		
		String[] producerFields = {"id", "name"}; 
		PRODUCER_REQUEST.setFields(producerFields);
	}
	
	/**
	 * Getting EditProduct page.
	 * @return ModelAndView
	 */
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public ModelAndView getPage(@PathVariable("id") final Long id) {
		ModelAndView modelAndView = new ModelAndView("editproduct");
		modelAndView.addObject("pageName", "Редагування виробу");
		
		ProductResource productResource = productFacade.getResource(id, PRODUCT_REQUEST);
		modelAndView.addObject("product", productResource);
		
		modelAndView.addObject("productTypesList", productTypeFacade.getResourcesList(PRODUCT_TYPE_REQUEST));
		
		modelAndView.addObject("categoriesList", categoryDictionaryFacade.getList());
		
		modelAndView.addObject("producersList", producerFacade.getResourcesList(PRODUCER_REQUEST));
		
		return modelAndView;
	}

}
