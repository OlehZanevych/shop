package org.shop.web.page.controller.addproduct;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.producer.ProducerResource;
import org.shop.resource.product.ProductResource;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.PagedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * AddProduct page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/addproduct")
public class AddProductPageController {
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> productFacade;
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> productTypeFacade;
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> categoryDictionaryFacade;
	
	@Resource(name = "producerFacade")
	private Facade<ProducerResource> producerFacade;
	
	private static final PagedRequest PRODUCT_TYPE_REQUEST = new PagedRequest();
	
	private static final PagedRequest PRODUCER_REQUEST = new PagedRequest();
	
	static {
		String[] productTypeFields = {"id", "name"}; 
		PRODUCT_TYPE_REQUEST.setFields(productTypeFields);
		
		String[] producerFields = {"id", "name"}; 
		PRODUCER_REQUEST.setFields(producerFields);
	}
	
	/**
	 * Getting AddProduct page.
	 * @return AddProduct page ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView modelAndView = new ModelAndView("addproduct");
		modelAndView.addObject("pageName", "Додавання виробу");
		
		modelAndView.addObject("productTypesList", productTypeFacade.getResourcesList(PRODUCT_TYPE_REQUEST));
		
		modelAndView.addObject("categoriesList", categoryDictionaryFacade.getList());
		
		modelAndView.addObject("producersList", producerFacade.getResourcesList(PRODUCER_REQUEST));
		
		return modelAndView;
	}

}
