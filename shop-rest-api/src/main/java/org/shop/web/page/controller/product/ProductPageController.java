package org.shop.web.page.controller.product;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.producer.ProducerResource;
import org.shop.resource.product.ProductResource;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.Request;
import org.shop.web.rest.constant.RequestConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Single page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/product")
public class ProductPageController {
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> productFacade;
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> productTypeFacade;
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> categoryDictionaryFacade;
	
	@Resource(name = "producerFacade")
	private Facade<ProducerResource> producerFacade;
	
	private Request productRequest = new Request();
	
	private Request productTypeRequest = new Request();
	
	private Request producerRequest = new Request();
	
	/**
	 * Default constructor.
	 */
	public ProductPageController() {
		String[] productFields = {"id", "productTypeId", "category", "producerId", "cost",
				"imageUri", "info"}; 
		productRequest.setFields(productFields);
		
		String[] productTypeFields = {"id", "name"}; 
		productTypeRequest.setFields(productTypeFields);
		
		String[] producerFields = {"id", "name"}; 
		producerRequest.setFields(producerFields);
	}
	
	/**
	 * Getting Single page.
	 * @return ModelAndView
	 */
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public ModelAndView getPage(@PathVariable("id") final Long id) {
		ModelAndView modelAndView = new ModelAndView("product");
		modelAndView.addObject("pageName", "Продукт");
		
		ProductResource productResource = productFacade.getResource(id, productRequest);
		modelAndView.addObject("product", productResource);
		
		Long productTypeId = productResource.getProductTypeId();
		String productTypeName = productTypeFacade.getResource(productTypeId, productTypeRequest).getName();
		modelAndView.addObject("productTypeName", productTypeName);
		
		Category category = productResource.getCategory();
		String categoryName = categoryDictionaryFacade.getEntry(category).getName();
		modelAndView.addObject("categoryName", categoryName);
		
		Long producerId = productResource.getProducerId();
		String producerName = producerFacade.getResource(producerId, producerRequest).getName();
		modelAndView.addObject("producerName", producerName);
		
		productResource.getProducerId();
		
		return modelAndView;
	}

}
