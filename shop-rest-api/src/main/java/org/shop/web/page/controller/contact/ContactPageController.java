package org.shop.web.page.controller.contact;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Contact page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/contact")
public class ContactPageController {
	
	/**
	 * Getting Contact page.
	 * @return Contact page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getPage(final ModelMap model) {
		model.addAttribute("pageName", "Контакти");
		return "contact";
	}

}
