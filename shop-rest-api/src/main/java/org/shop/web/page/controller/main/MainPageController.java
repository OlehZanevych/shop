package org.shop.web.page.controller.main;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.product.ProductResource;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.PagedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Main page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/main")
public class MainPageController {
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> productFacade;
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> productTypeFacade;
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> categoryDictionaryFacade;
	
	private PagedRequest productRequest = new PagedRequest();
	
	private PagedRequest productTypeRequest = new PagedRequest();
	
	/**
	 * Default constructor.
	 */
	public MainPageController() {
		String[] productFields = {"id", "productTypeId", "category", "cost", "imageUri"}; 
		productRequest.setFields(productFields);
		
		String[] productTypeFields = {"id", "name"}; 
		productTypeRequest.setFields(productTypeFields);
	}
	
	/**
	 * Getting Main page.
	 * @return Main page ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView modelAndView = new ModelAndView("main");
		modelAndView.addObject("productsList", productFacade.getResourcesList(productRequest));
		modelAndView.addObject("productTypesMap", productTypeFacade.getResourcesMap(productTypeRequest));
		modelAndView.addObject("categoriesMap", categoryDictionaryFacade.getMap());
		return modelAndView;
	}

}
