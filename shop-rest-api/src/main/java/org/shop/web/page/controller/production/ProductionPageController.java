package org.shop.web.page.controller.production;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.facade.facade.Facade;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.product.ProductResource;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.PagedRequest;
import org.shop.web.rest.constant.RequestConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Production page controller.
 * @author OlehZanevych
 */
@Controller
@RequestMapping("/production")
public class ProductionPageController {
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> productFacade;
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> productTypeFacade;
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> categoryDictionaryFacade;
	
	private static final String[] PRODUCT_FIELDS = {"id", "productTypeId", "category", "cost", "imageUri"}; 
	
	private PagedRequest productTypeRequest = new PagedRequest();
	
	/**
	 * Default constructor.
	 */
	public ProductionPageController() {
		String[] productTypeFields = {"id", "name"}; 
		productTypeRequest.setFields(productTypeFields);
	}
	
	/**
	 * Getting Production page.
	 * @return Production page ModelAndView
	 */
	@RequestMapping(value = RequestConstants.CATEGORY, method = RequestMethod.GET)
	public ModelAndView getPage(@PathVariable("category") final Category category) {
		PagedRequest productRequest = new PagedRequest();
		productRequest.setFields(PRODUCT_FIELDS);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("category", category.toString());
		productRequest.setParameters(parameters);
		
		ModelAndView modelAndView = new ModelAndView("production");
		modelAndView.addObject("pageName", "Продукція");
		modelAndView.addObject("currentCategory", category);
		modelAndView.addObject("productsList", productFacade.getResourcesList(productRequest));
		modelAndView.addObject("productTypesMap", productTypeFacade.getResourcesMap(productTypeRequest));
		modelAndView.addObject("categoriesMap", categoryDictionaryFacade.getMap());
		return modelAndView;
	}

}
