package org.shop.web.rest.controller.feedbackmessage;

import javax.annotation.Resource;

import org.shop.facade.facade.Facade;
import org.shop.resource.feedbackmessage.FeedbackMessageResource;
import org.shop.resource.message.MessageResource;
import org.shop.resource.message.MessageType;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles all API with FeedbackMessage entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/feedbackmessages")
public class FeedbackMessageController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(FeedbackMessageController.class);
	
	@Resource(name = "feedbackMessageFacade")
	private Facade<FeedbackMessageResource> facade;
	
	/**
	 * Method for creating new FeedbackMessageResource.
	 * @param feedbackMessageResource FeedbackMessageResource.
	 * @return FeedbackMessageResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public FeedbackMessageResource createResource(@RequestBody final FeedbackMessageResource feedbackMessageResource) {
		LOG.info("Creating feedbackMessage: {}", feedbackMessageResource);
		return facade.createResource(feedbackMessageResource);
	}
	
	/**
	 * Method for updating FeedbackMessageResource.
	 * @param id FeedbackMessageResource identifier.
	 * @param feedbackMessageResource updated FeedbackMessageResource.
	 * @return notification of update FeedbackMessageResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final FeedbackMessageResource feedbackMessageResource) {
		LOG.info("Updating feedbackMessage with id: {}, {}", id, feedbackMessageResource);
		facade.updateResource(id, feedbackMessageResource);
		return new MessageResource(MessageType.INFO, "FeedbackMessage Updated");
	}
	
	/**
	 * Method for getting FeedbackMessageResource.
	 * @param id FeedbackMessageResource identifier.
	 * @param request request
	 * @return FeedbackMessageResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public FeedbackMessageResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving feedbackMessage with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing FeedbackMessageResource.
	 * @param id FeedbackMessageResource identifier id.
	 * @return notification of deletion FeedbackMessageResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing feedbackMessage with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "FeedbackMessage removed");
	}
	
	/**
	 * Method that returns a page of FeedbackMessageResources.
	 * @param request request.
	 * @return page of FeedbackMessageResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResultResource<FeedbackMessageResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for FeedbackMessage Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
