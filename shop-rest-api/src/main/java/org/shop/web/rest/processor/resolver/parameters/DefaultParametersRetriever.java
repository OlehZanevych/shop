package org.shop.web.rest.processor.resolver.parameters;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.servlet.HandlerMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of Parameters Retriever.
 * @author OlehZanevych
 */
@Component("parametersRetriever")
public class DefaultParametersRetriever implements ParametersRetriever {

    @Override
    public Map<String, String> getParameters(final NativeWebRequest webRequest) {
        Map<String, String> resultMap = new HashMap<String, String>();

        Map<String, String> pathVariables = (Map<String, String>) webRequest.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST);
        Map<String, String> requestParams = getRequestParameterMap(webRequest);

        for (Map.Entry<String, String> entry : requestParams.entrySet()) {
            resultMap.put(entry.getKey(), entry.getValue());
        }

        resultMap.putAll(pathVariables);

        return resultMap;
    }

    /**
     * Method for getting parameteres from web request.
     * @param webRequest web request.
     * @return new web request.
     */
    private Map<String, String> getRequestParameterMap(final NativeWebRequest webRequest) {
        Map<String, String[]> parameters = webRequest.getParameterMap();
        Map<String, String> resultMap = new HashMap<>();

        for (Map.Entry<String, String[]> entry : parameters.entrySet()) {
            String[] value = entry.getValue();
            String newValue = getParameter(value);
            resultMap.put(entry.getKey(), newValue);
        }


        return resultMap;
    }

    /**
     * Method for getting parameter.
     * @param values values.
     * @return first element.
     */
    private String getParameter(final String[] values) {
        return values[0];
    }
}
