package org.shop.web.rest.controller.dictionary.category;

import java.util.List;

import javax.annotation.Resource;

import org.shop.model.enumtype.category.Category;
import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.resource.dictionary.DictionaryEntry;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles getting APIs for Category dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/categories")
public class CategoryDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDictionaryController.class);
	
	@Resource(name = "categoryDictionaryFacade")
	private DictionaryFacade<Category> dictionaryFacade;
	
	/**
	 * Method for getting entry from Category dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	public DictionaryEntry<Category> getEntry(@PathVariable("key") final Category key) {
		LOGGER.info("Retrieving entry from Category dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all Category dictionary.
	 * 
	 * @return Category dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public List<DictionaryEntry<Category>> getDictionary() {
		LOGGER.info("Retrieving all Category dictionary");
		return dictionaryFacade.getList();
	}
	
}
