package org.shop.web.rest.controller.cart;

import javax.annotation.Resource;

import org.shop.facade.facade.Facade;
import org.shop.resource.cart.CartResource;
import org.shop.resource.message.MessageResource;
import org.shop.resource.message.MessageType;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles all API with Cart entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/carts")
public class CartController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CartController.class);
	
	@Resource(name = "cartFacade")
	private Facade<CartResource> facade;
	
	/**
	 * Method for creating new CartResource.
	 * @param cartResource CartResource.
	 * @return CartResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public CartResource createResource(@RequestBody final CartResource cartResource) {
		LOG.info("Creating cart: {}", cartResource);
		return facade.createResource(cartResource);
	}
	
	/**
	 * Method for updating CartResource.
	 * @param id CartResource identifier.
	 * @param cartResource updated CartResource.
	 * @return notification of update CartResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final CartResource cartResource) {
		LOG.info("Updating cart with id: {}, {}", id, cartResource);
		facade.updateResource(id, cartResource);
		return new MessageResource(MessageType.INFO, "Cart Updated");
	}
	
	/**
	 * Method for getting CartResource.
	 * @param id CartResource identifier.
	 * @param request request
	 * @return CartResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public CartResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving cart with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing CartResource.
	 * @param id CartResource identifier id.
	 * @return notification of deletion CartResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing cart with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Cart removed");
	}
	
	/**
	 * Method that returns a page of CartResources.
	 * @param request request.
	 * @return page of CartResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResultResource<CartResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Cart Resources with request: {}", request);
		return facade.getResources(request);
	}
}
