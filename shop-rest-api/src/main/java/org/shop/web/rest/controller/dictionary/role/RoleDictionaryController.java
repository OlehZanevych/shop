package org.shop.web.rest.controller.dictionary.role;

import java.util.List;

import javax.annotation.Resource;

import org.shop.model.enumtype.role.Role;
import org.shop.facade.dictionary.DictionaryFacade;
import org.shop.resource.dictionary.DictionaryEntry;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles getting APIs for Role dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/roles")
public class RoleDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleDictionaryController.class);
	
	@Resource(name = "roleDictionaryFacade")
	private DictionaryFacade<Role> dictionaryFacade;
	
	/**
	 * Method for getting entry from Role dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	public DictionaryEntry<Role> getEntry(@PathVariable("key") final Role key) {
		LOGGER.info("Retrieving entry from Role dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all Role dictionary.
	 * 
	 * @return Role dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public List<DictionaryEntry<Role>> getDictionary() {
		LOGGER.info("Retrieving all Role dictionary");
		return dictionaryFacade.getList();
	}
	
}
