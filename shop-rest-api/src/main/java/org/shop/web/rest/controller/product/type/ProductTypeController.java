package org.shop.web.rest.controller.product.type;

import javax.annotation.Resource;

import org.shop.facade.facade.Facade;
import org.shop.resource.message.MessageResource;
import org.shop.resource.message.MessageType;
import org.shop.resource.product.type.ProductTypeResource;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles all API with ProductType entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/producttypes")
public class ProductTypeController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductTypeController.class);
	
	@Resource(name = "productTypeFacade")
	private Facade<ProductTypeResource> facade;
	
	/**
	 * Method for creating new ProductTypeResource.
	 * @param productTypeResource ProductTypeResource.
	 * @return ProductTypeResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public ProductTypeResource createResource(@RequestBody final ProductTypeResource productTypeResource) {
		LOG.info("Creating productType: {}", productTypeResource);
		return facade.createResource(productTypeResource);
	}
	
	/**
	 * Method for updating ProductTypeResource.
	 * @param id ProductTypeResource identifier.
	 * @param productTypeResource updated ProductTypeResource.
	 * @return notification of update ProductTypeResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final ProductTypeResource productTypeResource) {
		LOG.info("Updating ProductType with id: {}, {}", id, productTypeResource);
		facade.updateResource(id, productTypeResource);
		return new MessageResource(MessageType.INFO, "ProductType Updated");
	}
	
	/**
	 * Method for getting ProductTypeResource.
	 * @param id ProductTypeResource identifier.
	 * @param request request
	 * @return ProductTypeResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public ProductTypeResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving ProductType with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing ProductTypeResource.
	 * @param id ProductTypeResource identifier id.
	 * @return notification of deletion ProductTypeResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing ProductType with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "ProductType removed");
	}
	
	/**
	 * Method that returns a page of ProductTypeResources.
	 * @param request request.
	 * @return page of ProductTypeResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResultResource<ProductTypeResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for ProductType Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
