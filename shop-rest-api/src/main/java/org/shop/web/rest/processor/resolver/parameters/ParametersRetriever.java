package org.shop.web.rest.processor.resolver.parameters;

import org.springframework.web.context.request.NativeWebRequest;

import java.util.Map;

/**
 * Interface for extracting parameters extractor.
 * @author OlehZanevych
 */
public interface ParametersRetriever {

    /**
     * Method for getting parameters.
     * @param webRequest webRequest
     * @return Map of parameters.
     */
    Map<String, String> getParameters(final NativeWebRequest webRequest);
}
