package org.shop.web.rest.controller.product;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.shop.facade.facade.Facade;
import org.shop.model.product.Product;
import org.shop.resource.product.ProductResource;
import org.shop.resource.message.MessageResource;
import org.shop.resource.message.MessageType;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.service.EditService;
import org.shop.service.storage.StorageService;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller, that handles all API with Product entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/products")
public class ProductController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	private ServletContext servletContext;
	
	@Resource(name = "productFacade")
	private Facade<ProductResource> facade;
	
	@Resource(name = "productService")
	private EditService<Product> productService;
	
	@Resource(name = "fileSystemStorageService")
	private StorageService storageService;
	
	private String rootFolder;
	
	@Value("${upload.products.image.folder}")
	private String productsImageFolder;
	
	/**
	 * Initialization method.
	 */
	@PostConstruct
	protected void init() {
		rootFolder = servletContext.getRealPath("/");
	}
	
	/**
	 * Method for updating ProductResource image.
	 * @param id ProductResource identifier
	 * @param file multipart file
	 * @return notification of update ProductResource image
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.POST)
	public MessageResource updateFile(@PathVariable("id") final Long id,
			@RequestParam("file") final MultipartFile file) {
		
		LOG.info("Updating image for Product with id: {0}", id);
		
		Path folder = Paths.get(rootFolder + productsImageFolder);
		
		String storedFileName = storageService.store(file, folder, id);
		
		Product product = productService.getEntity(id);
		product.setImageUri("/" + productsImageFolder + "/" + storedFileName);
		productService.updateEntity(product);
		
		return new MessageResource(MessageType.INFO, "ProductResource image updated");
	}
	
	/**
	 * Method for creating new ProductResource.
	 * @param productResource ProductResource.
	 * @return ProductResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public ProductResource createResource(@RequestBody final ProductResource productResource) {
		LOG.info("Creating product: {}", productResource);
		return facade.createResource(productResource);
	}
	
	/**
	 * Method for updating ProductResource.
	 * @param id ProductResource identifier.
	 * @param productResource updated ProductResource.
	 * @return notification of update ProductResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final ProductResource productResource) {
		LOG.info("Updating product with id: {}, {}", id, productResource);
		facade.updateResource(id, productResource);
		return new MessageResource(MessageType.INFO, "Product Updated");
	}
	
	/**
	 * Method for getting ProductResource.
	 * @param id ProductResource identifier.
	 * @param request request
	 * @return ProductResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public ProductResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving product with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing ProductResource.
	 * @param id ProductResource identifier id.
	 * @return notification of deletion ProductResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing product with id: {}", id);
		facade.removeResource(id);
		Path folder = Paths.get(rootFolder + productsImageFolder);
		storageService.delete(folder, id);
		return new MessageResource(MessageType.INFO, "Product removed");
	}
	
	/**
	 * Method that returns a page of ProductResources.
	 * @param request request.
	 * @return page of ProductResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResultResource<ProductResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Product Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
