package org.shop.web.rest.controller.producer;

import javax.annotation.Resource;

import org.shop.facade.facade.Facade;
import org.shop.resource.producer.ProducerResource;
import org.shop.resource.message.MessageResource;
import org.shop.resource.message.MessageType;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles all API with Producer entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/producers")
public class ProducerController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProducerController.class);
	
	@Resource(name = "producerFacade")
	private Facade<ProducerResource> facade;
	
	/**
	 * Method for creating new ProducerResource.
	 * @param producerResource ProducerResource.
	 * @return ProducerResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public ProducerResource createResource(@RequestBody final ProducerResource producerResource) {
		LOG.info("Creating producer: {}", producerResource);
		return facade.createResource(producerResource);
	}
	
	/**
	 * Method for updating ProducerResource.
	 * @param id ProducerResource identifier.
	 * @param producerResource updated ProducerResource.
	 * @return notification of update ProducerResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final ProducerResource producerResource) {
		LOG.info("Updating producer with id: {}, {}", id, producerResource);
		facade.updateResource(id, producerResource);
		return new MessageResource(MessageType.INFO, "Producer Updated");
	}
	
	/**
	 * Method for getting ProducerResource.
	 * @param id ProducerResource identifier.
	 * @param request request
	 * @return ProducerResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public ProducerResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving producer with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing ProducerResource.
	 * @param id ProducerResource identifier id.
	 * @return notification of deletion ProducerResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing producer with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Producer removed");
	}
	
	/**
	 * Method that returns a page of ProducerResources.
	 * @param request request.
	 * @return page of ProducerResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResultResource<ProducerResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Producer Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
