package org.shop.web.rest.controller.user;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.shop.facade.facade.Facade;
import org.shop.model.session.Session;
import org.shop.resource.user.UserResource;
import org.shop.resource.message.MessageResource;
import org.shop.resource.message.MessageType;
import org.shop.resource.request.PagedRequest;
import org.shop.resource.request.PagedResultResource;
import org.shop.resource.request.Request;
import org.shop.web.rest.constant.RequestConstants;
import org.shop.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller, that handles all API with User entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/users")
public class UserController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	@Resource(name = "userFacade")
	private Facade<UserResource> facade;
	
	/**
	 * Method for creating new UserResource.
	 * @param userResource UserResource.
	 * @return UserResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public UserResource createResource(@RequestBody final UserResource userResource) {
		LOG.info("Creating user: {}", userResource);
		return facade.createResource(userResource);
	}
	
	/**
	 * Method for updating UserResource.
	 * @param id UserResource identifier.
	 * @param userResource updated UserResource.
	 * @param request HttpServletRequest.
	 * @param response HttpServletResponse.
	 * @return notification of update UserResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	public MessageResource updateResource(@PathVariable("id") final Long id,
			@RequestBody final UserResource userResource, final HttpServletRequest request,
			final HttpServletResponse response) {
		
		LOG.info("Updated user with id: {}, {}", id, userResource);
		facade.updateResource(id, userResource);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (id == ((Session) authentication.getDetails()).getUserId()) {
			new SecurityContextLogoutHandler().logout(request, response, authentication);
		}
		
		return new MessageResource(MessageType.INFO, "User Updated");
	}
	
	/**
	 * Method for getting UserResource.
	 * @param id UserResource identifier.
	 * @param request request
	 * @return UserResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	public UserResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving user with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing UserResource.
	 * @param id UserResource identifier id.
	 * @param request HttpServletRequest.
	 * @param response HttpServletResponse.
	 * @return notification of deletion UserResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	public MessageResource removeResource(@PathVariable("id") final Long id, final HttpServletRequest request,
			final HttpServletResponse response) {
		
		LOG.info("Removing user with id: {}", id);
		facade.removeResource(id);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (id == ((Session) authentication.getDetails()).getUserId()) {
			new SecurityContextLogoutHandler().logout(request, response, authentication);
		}
		
		return new MessageResource(MessageType.INFO, "User removed");
	}
	
	/**
	 * Method that returns a page of UserResources.
	 * @param request request.
	 * @return page of UserResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResultResource<UserResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for User Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
