package org.shop.json.deserializer;

/**
 * Common interface for JSON deserializer.
 * @author OlehZanevych
 */
public interface JSONDeserializer {

	/**
	 * Method to deserialize JSON content from given JSON content String.
	 * @param content content String
	 * @param clazz class
	 * @param <T> deserialized content class
	 * @return deserialized content
	 */
	<T> T deserialize(String content, Class<T> clazz);
	
}
