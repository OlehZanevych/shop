<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="contact">
		<div class="contact-form">
			<div class="container">
				<div class="col-md-6 contact-left">
					<h3>Бандерштат - магазин українських вишиванок</h3>
					<p><strong>Купити вишиванку</strong>&nbsp;– нелегке завдання, адже тут потрібно звертати увагу
					на якість тканини, значення орнаментів та кольорів. Якщо Ви прагнете знайти дійсно якісну сорочку,
					зверніть увагу на інтернет-магазин Бандерштат. Ми можемо гарантувати високу якість кожної вишиванки,
					адже співпрацюємо з перевіреними виробниками.</p>
					<p>У нас Ви зможете&nbsp;<strong>купити</strong>&nbsp;вишиванки у&nbsp;
					<strong>Львові дешево та сердито</strong>.</p>
					<div class="address">
						<div class=" address-grid">
							<i class="glyphicon glyphicon-map-marker"></i>
							<div class="address1">
								<h3>Адреса</h3>
								<p>вул. Університецька 47</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class=" address-grid">
							<i class="glyphicon glyphicon-phone"></i>
							<div class="address1">
								<h3>Телефон:</h3>
								<p>+123456789</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class=" address-grid">
							<i class="glyphicon glyphicon-envelope"></i>
							<div class="address1">
								<h3>Пошта:</h3>
								<p><a href="mailto:info@example.com">Oleh.Zanevych@gmail.com</a></p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class=" address-grid">
							<i class="glyphicon glyphicon-bell"></i>
							<div class="address1">
								<h3>Відкриті:</h3>
								<p>Понеділок-пятниця з 9 до 18</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="col-md-6 contact-top">
					<h3>Є питання? Не соромтесь, питайтесь</h3>
					<form id="feedbackForm">
						<div>
							<span>Ваше імя</span> <input id="senderName" type="text" value="" required autocomplete="off">
						</div>
						<div>
							<span>Ваш email</span> <input id="senderEmail" type="email" value="" required autocomplete="off">
						</div>
						<div>
							<span>Мета</span> <input id="subject" type="text" value="" required autocomplete="off">
						</div>
						<div>
							<span>Повідомлення</span> 
							<textarea id="description" required autocomplete="off"></textarea>
						</div><label class="hvr-skew-backward"><input type="submit" value="Надіслати"></label>
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		$('#feedbackForm').submit(function () {
			$.ajax({
				url: "${pageContext.request.contextPath}/api/feedbackmessages",
				type:"POST",
				data: JSON.stringify({
				    "senderName" : senderName.value,
				    "senderEmail" : senderEmail.value,
				    "subject" : subject.value,
				    "description" : description.value
				  }),
				contentType:"application/json; charset=utf-8",
				dataType:"json",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					alert("Фідбек успішно надіслано");
					location.href = "${pageContext.request.contextPath}/api/main";
				},
				error: function (qXHR, textStatus, errorThrown) {
					alert("Сталась помилка при надсиланні фідбеку");
				}
			});
		 	return false;
		});
	</script>
	
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>