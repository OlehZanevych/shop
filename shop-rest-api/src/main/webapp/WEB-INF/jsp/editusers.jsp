<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<script>
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		
		var usersURI = "${pageContext.request.contextPath}/api/users";
		
		function deleteUser(userId) {
			$.ajax({
				async: false,
				url: usersURI + "/" + userId,
				type: "DELETE",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					$("#user-" + userId).remove();
				},
				error: function (qXHR, textStatus, errorThrown) {
					alert("Сталась помилка при видаленні користувача");
				}
			});
		}
	</script>
	
	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
		        	<th>Логін</th>
		        	<th>П.І.Б.</th>
		        	<th>Email</th>
		        	<th>Операції</th>
				</tr>
		    </thead>
		    <tbody id="cart">
		    	<c:forEach var="user" items="${usersList}">
		    	<tr id="user-${user.id}">
		    		<td class="cart-item">${user.login}</td>
		    		<td class="cart-item">${user.name}</td>
		    		<td class="cart-item">${user.email}</td>
		    		<td>
		    			<a href="${pageContext.request.contextPath}/api/edituser/${user.id}"><img alt="Редагувати" title="Редагувати" src="${pageContext.request.contextPath}/resources/images/edit.png"></a>
						<img alt="Видалити" title="Видалити" src="${pageContext.request.contextPath}/resources/images/delete.png" onclick="deleteUser(${user.id})">
		    		</td>
		    	</tr>
		    	</c:forEach>
		    </tbody>
		</table>
	</div>
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>