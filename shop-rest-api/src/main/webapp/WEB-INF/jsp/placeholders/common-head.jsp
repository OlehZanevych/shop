<%@ page contentType="text/html; charset=UTF-8" %>
<title>Бандерштат - інтернет магазин українських вишиванок</title>
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" media="all" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="${pageContext.request.contextPath}/resources/css/style.css" media=
	"all" rel="stylesheet" type="text/css">
<!--//theme-style-->
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<script type="application/x-javascript">
	addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jstarbox.js"></script>
<link charset="utf-8" href=
	"${pageContext.request.contextPath}/resources/css/jstarbox.css" media="screen"
	rel="stylesheet" type="text/css">
</script><!---//End-rate==-->
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<meta name="_csrf" content="${_csrf.token}"/>