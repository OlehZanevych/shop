<%@ page contentType="text/html; charset=UTF-8" %>
<jsp:include page="base-header.jsp"/>
<div class="banner-top">
	<div class="container">
		<h1>${pageName}</h1>
		<h2><a href="${pageContext.request.contextPath}/api/main">Головна</a><label>/${pageName}</label></h2>
	</div>
</div>