<%@ page contentType="text/html; charset=UTF-8" %>
<div class="hspace"></div>
<div class="footer">
	<div class="footer-bottom">
		<div class="container">
			<ul class="footer-bottom-top">
				<li>
					<a href="#"><img alt="" class="img-responsive" src=
					"${pageContext.request.contextPath}/resources/images/f1.png"></a>
				</li>
				<li>
					<a href="#"><img alt="" class="img-responsive" src=
					"${pageContext.request.contextPath}/resources/images/f2.png"></a>
				</li>
				<li>
					<a href="#"><img alt="" class="img-responsive" src=
					"${pageContext.request.contextPath}/resources/images/f3.png"></a>
				</li>
			</ul>
			<p class="footer-class">&copy; 2016 Бандерштат | Всі права захищені</p>
			<div class="clearfix"></div>
		</div>
	</div>
</div>