<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="header">
	<div class="container">
		<div class="head">
			<div class=" logo">
				<a href="${pageContext.request.contextPath}/api/main"><img alt="" src="${pageContext.request.contextPath}/resources/images/logo.png"></a>
			</div>
		</div>
	</div>
	<div class="header-top">
		<div class="container">
			<div class="col-sm-5 col-md-offset-2 header-login">
				<ul>
					<sec:authorize access="isAuthenticated()">
	   					<li>
							<a href="${pageContext.request.contextPath}/api/logout">Вихід</a>
						</li>
						<sec:authorize access="hasAuthority('MODERATOR')">
							<li>
								<a href="${pageContext.request.contextPath}/api/editproducts">Редагувати продукцію</a>
							</li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('ADMIN')">
							<li>
								<a href="${pageContext.request.contextPath}/api/editusers">Редагувати користувачів</a>
							</li>
						</sec:authorize>
					</sec:authorize>
					<sec:authorize access="!isAuthenticated()">
	   					<li>
							<a href="${pageContext.request.contextPath}/api/login">Вхід</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/api/registration">Реєстрація</a>
						</li>
					</sec:authorize>
				</ul>
			</div>
			<div class="col-sm-5 header-social">
				<ul>
					<li>
						<a href="#"><i></i></a>
					</li>
					<li>
						<a href="#"><i class="ic1"></i></a>
					</li>
					<li>
						<a href="#"><i class="ic2"></i></a>
					</li>
					<li>
						<a href="#"><i class="ic3"></i></a>
					</li>
					<li>
						<a href="#"><i class="ic4"></i></a>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div class="head-top">
			<div class="col-sm-8 col-md-offset-2 h_menu4">
				<nav class="navbar nav_bottom" role="navigation">
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
						<ul class="nav navbar-nav nav_1">
							<li>
								<a class="color" href="${pageContext.request.contextPath}/api/production/MAN">Чоловіки</a>
							</li>
							<li>
								<a class="color" href="${pageContext.request.contextPath}/api/production/WOMAN">Жінки</a>
							</li>
							<li>
								<a class="color" href="${pageContext.request.contextPath}/api/production/BOY">Хлопчики</a>
							</li>
							<li>
								<a class="color" href="${pageContext.request.contextPath}/api/production/GIRL">Дівчатка</a>
							</li>
							<li>
								<a class="color" href="${pageContext.request.contextPath}/api/contact">Контакти</a>
							</li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
			<div class="col-sm-2 search-right">
				<sec:authorize access="isAuthenticated() && !hasAuthority('BAN')">
					<div class="cart box_1">
						<a href="${pageContext.request.contextPath}/api/checkout">
						<h3></h3>
						<div class="total">
							<h3><img alt="" src=
							"${pageContext.request.contextPath}/resources/images/cart.png"></h3>
						</div></a>
					</div>
				</sec:authorize>
				<div class="clearfix"></div><!--=-->
				<!---pop-up-box==-->
				<link href="${pageContext.request.contextPath}/resources/css/popuo-box.css" media="all" rel="stylesheet" type=
				"text/css">
				<script src="${pageContext.request.contextPath}/resources/js/jquery.magnific-popup.js" type="text/javascript">
				</script> <!---//pop-up-box==-->
				<div class="mfp-hide" id="small-dialog">
					<div class="search-top">
						<div class="login-search">
							<input type="submit" value=""> <input onblur=
							"if (this.value == '') {this.value = 'Пошук..';}" onfocus=
							"this.value = '';" type="text" value="Пошук..">
						</div>
						<p>Бандерштат</p>
					</div>
				</div>
				<script>
				                                      $(document).ready(function() {
				                                      $('.popup-with-zoom-anim').magnificPopup({
				                                      type: 'inline',
				                                      fixedContentPos: false,
				                                      fixedBgPos: true,
				                                      overflowY: 'auto',
				                                      closeBtnInside: true,
				                                      preloader: false,
				                                      midClick: true,
				                                      removalDelay: 300,
				                                      mainClass: 'my-mfp-zoom-in'
				                                      });
				                                                                                                                  
				                                      });
				</script> <!--=-->
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>