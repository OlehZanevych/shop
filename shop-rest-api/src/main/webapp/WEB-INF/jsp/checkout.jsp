<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
		        	<th>Продукт</th>
		        	<th>Виробник</th>
		        	<th>Кількість</th>
		        	<th>Ціна</th>
				</tr>
		    </thead>
		    <tbody id="cart"></tbody>
		</table>
	  
	<script>
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
						
		var currentSessionsURI = "${pageContext.request.contextPath}/api/sessions/current";
		
		var rootCartsURI = "${pageContext.request.contextPath}/api/carts";	
						
		var cartsURI = rootCartsURI + "?fields=id,productTypeName,producerName,count,cost&userId=";				
						
		var currentUserId = 0;
						
		$.ajax({
			async: false,
			type: 'GET',
			url: currentSessionsURI,
			success: function(message, textStatus, jqXHR) {
				currentUserId = message.userId;
			}
		});
		
		var products = [];
		
		$.ajax({
			async: false,
			type: 'GET',
			url: cartsURI + currentUserId,
			success: function(message, textStatus, jqXHR) {
				products = message.resources;
					 
			}
		});
		
		var total = 0;
		
		products.forEach(function (product) {
			var row = document.createElement("tr");
			
			var productTypeNameCell = document.createElement("td");
			var productTypeNameTextNode = document.createTextNode(product.productTypeName);
			productTypeNameCell.appendChild(productTypeNameTextNode);
			productTypeNameCell.className = "cart-item";
			row.appendChild(productTypeNameCell);
			
			var producerNameCell = document.createElement("td");
			var  producerNameTextNode = document.createTextNode(product.producerName);
			producerNameCell.appendChild(producerNameTextNode);
			producerNameCell.className = "cart-item";
			row.appendChild(producerNameCell);
			
			var countCell = document.createElement("td");
			var countTextNode = document.createTextNode(product.count);
			countCell.appendChild(countTextNode);
			countCell.className = "cart-item";
			row.appendChild(countCell);
			
			var priceCell = document.createElement("td");
			var price = product.count * product.cost;
			total = total + price;
			var priceTextNode = document.createTextNode(price + " грн. (по " + product.cost + " грн. за шт.)");
			priceCell.appendChild(priceTextNode);
			priceCell.className = "cart-item";
			row.appendChild(priceCell);
			
			cart.appendChild(row);
		});
		
		var totalRow = document.createElement("tr");
		var totalCell = document.createElement("td");
		var totalTextNode = document.createTextNode("Усього " + total + " грн.");
		totalCell.appendChild(totalTextNode);
		totalCell.colSpan = 4;
		totalCell.className = "cart-total-item";
		totalRow.appendChild(totalCell);
		cart.appendChild(totalRow);
						
						
		function deleteCart() {
			products.forEach(function (product) {
				$.ajax({
					async: false,
					url: rootCartsURI + "/" + product.id,
					type: "DELETE",
					beforeSend: function (request) {
						request.setRequestHeader(CSRFHeader, CSRFToken);
					},
					error: function (qXHR, textStatus, errorThrown) {
						alert("Сталась помилка при сплаті покупки: " + JSON.stringify(qXHR.responseJSON));
					}
				});
			});
			location.reload();
		}
						
						
	</script>
	  
	<input class="add-to item_add hvr-skew-backward" type="button" value="Оплатити" onclick="deleteCart()" />
</div>
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>