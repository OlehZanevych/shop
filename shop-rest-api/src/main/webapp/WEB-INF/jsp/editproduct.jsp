<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="single">
		<div class="container">
			<div class="col-md-5 grid">
				<div class="flexslider">
					<img id="productImage" class="img-responsive" data-imagezoom="true" src="${pageContext.request.contextPath}${product.imageUri}">
				</div>
				<button id="choose-photo" class="choose-button">Вибрати фото</button>
			</div>
			<div class="col-md-7 single-top-in">
				<form id="editProductForm">
					<div class="span_2_of_a1 simpleCart_shelfItem">
						<select id="productType" class="admin-edit-item selectpicker">
							<c:forEach var="productType" items="${productTypesList}">
							    <option value="${productType.id}"
							    <c:if test="${productType.id == product.productTypeId}">selected="selected"</c:if>>
							    ${productType.name}</option>
							</c:forEach>
						</select>
						<select id="category" class="admin-edit-item selectpicker">
							<c:forEach var="category" items="${categoriesList}">
								<option value="${category.key}"
								<c:if test="${category.key == product.category}">selected="selected"</c:if>>
								${category.name}</option>
							</c:forEach>
						</select>
						<select id="producer" class="admin-edit-item selectpicker">
							<c:forEach var="producer" items="${producersList}">
								<option value="${producer.id}"
								<c:if test="${producer.id == product.producerId}">selected="selected"</c:if>>
								${producer.name}</option>
							</c:forEach>
						</select>
						<input id="cost" class="admin-edit-item login-mail" placeholder="Ціна (грн.)" required="" type="text" value="${product.cost}" autocomplete="off">
						<textarea id="info" class="admin-edit-item edit-textarea">${product.info}</textarea>
						<input class="add-to item_add hvr-skew-backward" type="submit" value="Зберегти зміни" />
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div><!---->
				</form>
			</div><!--=-->
			<div class="clearfix"></div>
		</div>
	</div><!--//content-->
	<!--footer-->
	
	<script type="text/javascript">
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		var uri = "${pageContext.request.contextPath}/api/products/${product.id}";
		var fileUpdated = false;
		var loadFile = function(event) {
	    	var reader = new FileReader();
	    	reader.onload = function(){
	      		productImage.src = reader.result;
	      		fileUpdated = true;
	    	}
	    	reader.readAsDataURL(event.target.files[0]);
	  	}
		var input = document.createElement('input');
		input.type = 'file';
		input.accept = "image/*";
		input.onchange = loadFile;
		$('#choose-photo').click(function() {
			input.click();
		});
		$('#editProductForm').submit(function () {
			$.ajax({
				url: uri,
				type:"PUT",
				data: JSON.stringify({
					"productTypeId" : productType.value,
				    "category" : category.value,
				    "producerId" : producer.value,
				    "cost" : cost.value,
				    "info" : info.value
				}),
				contentType:"application/json; charset=utf-8",
				dataType:"json",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					if (fileUpdated) {
						var formData = new FormData();
						formData.append('file', input.files[0]);
						$.ajax({
							url: uri,
							type: "POST",
							data: formData,
							contentType: false,
							processData: false,
							cache: false,
							beforeSend: function (request) {
				                request.setRequestHeader(CSRFHeader, CSRFToken);
				            },
							success: function (message, textStatus, jqXHR) {
								location.reload();
							},
							error: function (qXHR, textStatus, errorThrown) {
								alert("Сталась помилка при оновлені картинки: " + JSON.stringify(qXHR));
								location.reload();
							}
						});
					} else {
						location.reload();
					}
				},
				error: function (qXHR, textStatus, errorThrown) {
					alert("Сталась помилка при оновлені контенту: " + JSON.stringify(qXHR));
					location.reload();
				}
			});
		 	return false;
		});
	</script>
	<jsp:include page="placeholders/footer.jsp"/>
	<script src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
</body>
</html>