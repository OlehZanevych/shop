<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="single">
		<div class="container">
			<div class="col-md-5 grid">
				<div class="flexslider">
					<img class="img-responsive" data-imagezoom="true" src="${pageContext.request.contextPath}${product.imageUri}">
				</div>
			</div>
			<div class="col-md-7 single-top-in">
				<div class="span_2_of_a1 simpleCart_shelfItem">
					<h3>${productTypeName}</h3>
					<h4>${categoryName}</h4>
					<h4>${producerName}<h4>
					<p class="item_price">${product.cost} грн.</p>
						
					<!--<h4 class="quick">Короткий опис</h4>-->
					<p class="quick_desc">${product.info}</p>
					
					<sec:authorize access="hasAuthority('CUSTOMER')">
						<div class="quantity">
							<div class="quantity-select">
								<div class="entry value-minus">
									&nbsp;
								</div>
								<div id="divUpd" class="entry value">
									<span>0</span>
								</div>
								<div class="entry value-plus active">
									&nbsp;
								</div>
							</div>
						</div><!--quantity-->
						<script>
							var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
							var CSRFToken = $("meta[name='_csrf']").attr("content");
							
							var currentSessionsURI = "${pageContext.request.contextPath}/api/sessions/current";
							
							var cartsURI = "${pageContext.request.contextPath}/api/carts";
							
							var divUpd = $('.value-minus').parent().find('.value');
							
							var currentUserId = 0;
							
							var cartId = 0;
							
							$.ajax({
							     async: false,
							     type: 'GET',
							     url: currentSessionsURI,
							     success: function(message, textStatus, jqXHR) {
							    	 currentUserId = message.userId;
							     }
							});
							
							$.ajax({
							     async: false,
							     type: 'GET',
							     url: cartsURI + "?productId=${product.id}&userId=" + currentUserId,
							     success: function(message, textStatus, jqXHR) {
							    	 if (message.count === 1) {
							    		 var cart = message.resources[0];
							    		 cartId = cart.id;
							    		 divUpd.text(cart.count);
							    	 };
							     }
							});
							
							function createCart() {
								$.ajax({
									async: false,
									url: cartsURI,
									type: "POST",
									data: JSON.stringify({
										"productId" : ${product.id},
									    "count" : 1,
									    "userId" : currentUserId
									}),
									contentType:"application/json; charset=utf-8",
									dataType:"json",
									beforeSend: function (request) {
						                request.setRequestHeader(CSRFHeader, CSRFToken);
						            },
									success: function (message, textStatus, jqXHR) {
										cartId = message.id;
										divUpd.text(1);
									},
									error: function (qXHR, textStatus, errorThrown) {
										alert("Сталась помилка при оновлені кількості товару: " + JSON.stringify(qXHR.responseJSON));
									}
								});
							}
							
							function updateCart(newVal) {
								$.ajax({
									async: false,
									url: cartsURI + "/" + cartId,
									type: "PUT",
									data: JSON.stringify({
									    "count" : newVal
									}),
									contentType:"application/json; charset=utf-8",
									dataType:"json",
									beforeSend: function (request) {
						                request.setRequestHeader(CSRFHeader, CSRFToken);
						            },
									success: function (message, textStatus, jqXHR) {
										divUpd.text(newVal);
									},
									error: function (qXHR, textStatus, errorThrown) {
										alert("Сталась помилка при оновлені кількості товару: " + JSON.stringify(qXHR.responseJSON));
									}
								});
							}
							
							function deleteCart() {
								$.ajax({
									async: false,
									url: cartsURI + "/" + cartId,
									type: "DELETE",
									beforeSend: function (request) {
						                request.setRequestHeader(CSRFHeader, CSRFToken);
						            },
									success: function (message, textStatus, jqXHR) {
										divUpd.text(0);
										cartId = 0;
									},
									error: function (qXHR, textStatus, errorThrown) {
										alert("Сталась помилка при оновлені кількості товару: " + JSON.stringify(qXHR.responseJSON));
									}
								});
							}
							
							$('.value-plus').on('click', function() {
								var newVal = parseInt(divUpd.text(), 10) + 1;
								if (newVal === 1) {
									createCart();
								} else {
									updateCart(newVal);
								}
							});
		
							$('.value-minus').on('click', function(){
								 var newVal = parseInt(divUpd.text(), 10) - 1;
								 if (newVal > 0) {
									 updateCart(newVal);
								 }
								 if(newVal >= 0) {
									 if (newVal > 0) {
										 updateCart(newVal);
									 } else {
										 deleteCart();
									 }
								 }
							});
						</script>
					</sec:authorize>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div><!--//content-->
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
	<script src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
</body>
</html>