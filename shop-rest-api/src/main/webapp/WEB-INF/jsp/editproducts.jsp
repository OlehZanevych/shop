<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<script>
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		
		var productsURI = "${pageContext.request.contextPath}/api/products";
		
		function deleteProduct(productId) {
			$.ajax({
				async: false,
				url: productsURI + "/" + productId,
				type: "DELETE",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					$("#product-" + productId).remove();
				},
				error: function (qXHR, textStatus, errorThrown) {
					alert("Сталась помилка при видаленні товарів: " + JSON.stringify(qXHR.responseJSON));
				}
			});
		}
	</script>
	
	<div class="container">
		<div class="content-mid">
			<h3>Уся продукція</h3><label class="line"></label>
			<div class="mid-popular">
				<c:forEach var="product" items="${productsList}">
					<div id="product-${product.id}" class="col-md-3 item-grid1 simpleCart_shelfItem">
						<div class=" mid-pop">
							<div class="pro-img">
								<img alt="" class="img-responsive" src="${pageContext.request.contextPath}${product.imageUri}">
								<div class="zoom-icon">
									<a class="b-link-stripe b-animate-go thickbox" href="${pageContext.request.contextPath}${product.imageUri}"
									rel="title"><i class="glyphicon glyphicon-search icon"></i></a>
									<a href="${pageContext.request.contextPath}/api/product/${product.id}"><i class=
									"glyphicon glyphicon-menu-right icon"></i></a>
								</div>
							</div>
							<div class="mid-1">
								<div class="women">
									<div class="women-top">
										<span>${categoriesMap.get(product.category).name}</span>
										<h6>${productTypesMap.get(product.productTypeId).name}</h6>
									</div>
									<div class="img item_add">
										<a href="${pageContext.request.contextPath}/api/editproduct/${product.id}"><img alt="" src="${pageContext.request.contextPath}/resources/images/edit.png"></a>
										<button class="img-button" onclick="deleteProduct(${product.id})"><img alt="" src="${pageContext.request.contextPath}/resources/images/delete.png"></button>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="mid-2">
									<p><em class="item_price">${product.cost} грн.</em></p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="clearfix"></div>
		<a href="${pageContext.request.contextPath}/api/addproduct"><input class="add-to item_add hvr-skew-backward" type="button" value="Додати товар" /></a>
	</div>
	
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>