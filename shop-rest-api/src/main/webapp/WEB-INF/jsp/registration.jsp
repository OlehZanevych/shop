<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="container">
		<div class="login">
			<form id="registrationForm">
				<div class="col-md-6 login-do">
					<div class="login-mail">
						<input id="surname" placeholder="Прізвище" required type="text"> <i class="glyphicon glyphicon-user"></i>
					</div>
					<div class="login-mail">
						<input id="firstName" placeholder="Ім'я" required type="text"> <i class="glyphicon glyphicon-user"></i>
					</div>
					<div class="login-mail">
						<input id="middleName" placeholder="По батькові" required type="text"> <i class="glyphicon glyphicon-user"></i>
					</div>
					<div class="login-mail">
						<input id="email" placeholder="Пошта" required type="email"> <i class="glyphicon glyphicon-envelope"></i>
					</div>
					<div class="login-mail">
						<input id="phone" placeholder="Телефон" type="text"> <i class="glyphicon glyphicon-phone"></i>
					</div>
					<div class="login-mail">
						<input id="login" placeholder="Логін" required type="text"> <i class="glyphicon glyphicon-user"></i>
					</div>
					<div class="login-mail">
						<input id="password" placeholder="Пароль" required type="password"> <i class="glyphicon glyphicon-lock"></i>
					</div>
					<textarea id="info" placeholder="Напишіть тут трішки про себе..." class="admin-edit-item edit-textarea"></textarea>
					<div class="hspace"></div>
					<label class="hvr-skew-backward"><input type="submit" value="Зареєструватись"></label>
				</div>
				<div class="col-md-6 login-right">
					<h3>Авторизуватись на сайті</h3>
					<p><strong>Купити вишиванку</strong>&nbsp;– нелегке завдання, адже тут потрібно звертати увагу
					на якість тканини, значення орнаментів та кольорів. Якщо Ви прагнете знайти дійсно якісну сорочку,
					зверніть увагу на інтернет-магазин Бандерштат. Ми можемо гарантувати високу якість кожної вишиванки,
					адже співпрацюємо з перевіреними виробниками.</p>
					<p>У нас Ви зможете&nbsp;<strong>купити</strong>&nbsp;вишиванки у&nbsp;
					<strong>Львові дешево та сердито</strong>.</p>
					<a class="hvr-skew-backward" href="login">Увійти</a>
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
	
	<script type="text/javascript">
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		$('#registrationForm').submit(function () {
			$.ajax({
				url: "${pageContext.request.contextPath}/api/users",
				type:"POST",
				data: JSON.stringify({
				    "login" : login.value,
				    "password" : password.value,
				    "surname" : surname.value,
				    "firstName" : firstName.value,
				    "middleName" : middleName.value,
				    "email" : email.value,
				    "phone" : phone.value,
				    "info" : info.value
				  }),
				contentType:"application/json; charset=utf-8",
				dataType:"json",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					alert("Ви успішно зареєструвались. Тепер можете увійти на сайт");
					location.href = "${pageContext.request.contextPath}/api/login";
				},
				error: function (qXHR, textStatus, errorThrown) {
					var message = qXHR.responseJSON.message;
					if (message) {
						if (message === "ConstraintViolationName : t_system_users_login_key") {
							alert("На жаль, логін " + login.value + " вже зайнятий. Будь ласка, придумайте інший");
						}
					} else {
						alert("Сталась помилка при реєстрації користувача");
					}
				}
			});
		 	return false;
		});
	</script>
	
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>