<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
	<link href="${pageContext.request.contextPath}/resources/css/form.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="product">
		<div class="container">
			<div class="col-md-9">
				<div class="mid-popular">
					<c:forEach var="product" items="${productsList}">
						<div class="col-md-4 item-grid1 simpleCart_shelfItem">
							<div class=" mid-pop">
								<div class="pro-img">
									<img alt="" class="img-responsive" src="${pageContext.request.contextPath}${product.imageUri}">
									<div class="zoom-icon">
										<a class="b-link-stripe b-animate-go thickbox" href="${pageContext.request.contextPath}${product.imageUri}"
										rel="title"><i class="glyphicon glyphicon-search icon"></i></a>
										<a href="${pageContext.request.contextPath}/api/product/${product.id}"><i class=
										"glyphicon glyphicon-menu-right icon"></i></a>
									</div>
								</div>
								<div class="mid-1">
									<div class="women">
										<div class="women-top">
											<h6>${productTypesMap.get(product.productTypeId).name}</h6>
										</div>
										<div class="img item_add">
											<a href="${pageContext.request.contextPath}/api/product/${product.id}"><img alt="" src="${pageContext.request.contextPath}/resources/images/ca.png"></a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="mid-2">
										<em class="item_price">${product.cost} грн.</em></p>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-3 product-bottom">
				<!--categories-->
				<section class="sky-form">
					<h4 class="cate">Категорії</h4>
					<div class="row row1 scroll-pane">
						<div class="col col-4 category-menu">
							<c:forEach var="entry" items="${categoriesMap}">
								<c:choose> 
									<c:when test="${currentCategory == entry.key}">
								    	<label>${entry.value.name}</label>
								  	</c:when>
								  	<c:otherwise>
								  		<a href="${entry.key}">${entry.value.name}</a>
								  	</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>