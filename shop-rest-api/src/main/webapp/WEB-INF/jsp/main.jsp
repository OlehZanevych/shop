<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/base-header.jsp"/>
	
	<!--banner-->
	<div class="banner">
		<div class="container">
			<section class="rw-wrapper">
				<h1 class="rw-sentence"><span>Українські вишиванки</span></h1>
				<div class="rw-words rw-words-1">
					<div class="rw-words rw-words-2">
						<h1 class="rw-sentence">
						<span>Якщо Ви прагнете знайти</span>
						<span>дійсно якісну вишивку,</span>
						<span>зверніть увагу</span>
						<span>на наш інтернет-магазин</span>
						<span>Бандерштат</span>
						<span>Непожалієте... чесно))</span>
						</h1>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!--content-->
	<div class="content">
		<div class="container">
			<div class="content-top">
				<div class="col-md-6 col-md item-grid1">
					<div class="col-3">
						<a href="${pageContext.request.contextPath}/api/production/MAN"><img alt="" class="img-responsive" src=
						"${pageContext.request.contextPath}/resources/images/pi1.jpg">
						<div class="col-pic">
							<p>Бандерштат</p><label></label>
							<h5>Для чоловіків</h5>
						</div></a>
					</div>
					<div class="col-3">
						<a href="${pageContext.request.contextPath}/api/production/WOMAN"><img alt="" class="img-responsive" src=
						"${pageContext.request.contextPath}/resources/images/pi2.jpg">
						<div class="col-pic">
							<p>Бандерштат</p><label></label>
							<h5>Для жінок</h5>
						</div></a>
					</div>
				</div>
				<div class="col-md-6 col-md item-grid1">
					<div class="col-3">
						<a href="${pageContext.request.contextPath}/api/production/BOY"><img alt="" class="img-responsive" src=
						"${pageContext.request.contextPath}/resources/images/pi3.jpg">
						<div class="col-pic">
							<p>Бандерштат</p><label></label>
							<h5>Для хлопчиків</h5>
						</div></a>
					</div>
					<div class="col-3">
						<a href="${pageContext.request.contextPath}/api/production/GIRL"><img alt="" class="img-responsive" src=
						"${pageContext.request.contextPath}/resources/images/pi4.jpg">
						<div class="col-pic">
							<p>Бандерштат</p><label></label>
							<h5>Для дівчаток</h5>
						</div></a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div><!--products-->
			<div class="content-mid">
				<h3>Уся продукція</h3><label class="line"></label>
				<div class="mid-popular">
					<c:forEach var="product" items="${productsList}">
						<div class="col-md-3 item-grid1 simpleCart_shelfItem">
							<div class=" mid-pop">
								<div class="pro-img">
									<img alt="" class="img-responsive" src="${pageContext.request.contextPath}${product.imageUri}">
									<div class="zoom-icon">
										<a class="b-link-stripe b-animate-go thickbox" href="${pageContext.request.contextPath}${product.imageUri}"
										rel="title"><i class="glyphicon glyphicon-search icon"></i></a>
										<a href="${pageContext.request.contextPath}/api/product/${product.id}"><i class=
										"glyphicon glyphicon-menu-right icon"></i></a>
									</div>
								</div>
								<div class="mid-1">
									<div class="women">
										<div class="women-top">
											<span>${categoriesMap.get(product.category).name}</span>
											<h6>${productTypesMap.get(product.productTypeId).name}</h6>
										</div>
										<div class="img item_add">
											<a href="${pageContext.request.contextPath}/api/product/${product.id}"><img alt="" src="${pageContext.request.contextPath}/resources/images/ca.png"></a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="mid-2">
										<p><em class="item_price">${product.cost} грн.</em></p>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
			<!--//products-->
		</div>
	</div><!--//content-->
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>