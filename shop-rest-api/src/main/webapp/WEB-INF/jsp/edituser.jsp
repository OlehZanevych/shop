<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<script>
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		
		var productsURI = "${pageContext.request.contextPath}/api/products";
		
		function deleteProduct(productId) {
			$.ajax({
				async: false,
				url: productsURI + "/" + productId,
				type: "DELETE",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					$("#product-" + productId).remove();
				},
				error: function (qXHR, textStatus, errorThrown) {
					alert("Сталась помилка при видаленні товарів: " + JSON.stringify(qXHR.responseJSON));
				}
			});
		}
	</script>
	
	<div class="container data-container">
		<form id="userForm">
			<div class="login-do">
				<div class="login-mail">
					<input id="surname" placeholder="Прізвище" required type="text" value="${user.surname}"> <i class="glyphicon glyphicon-user"></i>
				</div>
				<div class="login-mail">
					<input id="firstName" placeholder="Ім'я" required type="text" value="${user.firstName}"> <i class="glyphicon glyphicon-user"></i>
				</div>
				<div class="login-mail">
					<input id="middleName" placeholder="По батькові" required type="text" value="${user.middleName}"> <i class="glyphicon glyphicon-user"></i>
				</div>
				<div class="login-mail">
					<input id="email" placeholder="Пошта" required type="email" value="${user.email}"> <i class="glyphicon glyphicon-envelope"></i>
				</div>
				<div class="login-mail">
					<input id="phone" placeholder="Телефон" type="text" value="${user.phone}"> <i class="glyphicon glyphicon-phone"></i>
				</div>
				<div class="login-mail">
					<input id="login" placeholder="Логін" required type="text" value="${user.login}"> <i class="glyphicon glyphicon-user"></i>
				</div>
				<div class="login-mail">
					<input id="password" placeholder="Пароль" type="password" value=""> <i class="glyphicon glyphicon-lock"></i>
				</div>
				<select id="role" class="admin-edit-item selectpicker">
					<c:forEach var="role" items="${rolesList}">
						<option value="${role.key}"
						<c:if test="${role.key == user.role}">selected="selected"</c:if>>
						${role.name}</option>
					</c:forEach>
				</select>
				<textarea id="info" placeholder="Інша інформація про користувача..." class="admin-edit-item edit-textarea">${user.info}</textarea>
				<div class="hspace"></div>
				<label class="hvr-skew-backward"><input type="submit" value="Зберегти зміни"></label>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
		var CSRFHeader = $("meta[name='_csrf_header']").attr("content");
		var CSRFToken = $("meta[name='_csrf']").attr("content");
		var newPassword = password.value;
		if (newPassword === "") {
			newPassword = null;
		}
		$('#userForm').submit(function () {
			$.ajax({
				url: "${pageContext.request.contextPath}/api/users/${user.id}",
				type:"PUT",
				data: JSON.stringify({
				    "login" : login.value,
				    "password" : newPassword,
				    "surname" : surname.value,
				    "firstName" : firstName.value,
				    "middleName" : middleName.value,
				    "email" : email.value,
				    "phone" : phone.value,
				    "role" : role.value,
				    "info" : info.value
				  }),
				contentType:"application/json; charset=utf-8",
				dataType:"json",
				beforeSend: function (request) {
	                request.setRequestHeader(CSRFHeader, CSRFToken);
	            },
				success: function (message, textStatus, jqXHR) {
					location.reload();
				},
				error: function (qXHR, textStatus, errorThrown) {
					var message = qXHR.responseJSON.message;
					if (message) {
						if (message === "ConstraintViolationName : t_system_users_login_key") {
							alert("На жаль, логін " + login.value + " вже зайнятий. Будь ласка, придумайте інший");
						}
					} else {
						alert("Сталась помилка при реєстрації користувача");
					}
				}
			});
		 	return false;
		});
	</script>
	
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>