<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="placeholders/common-head.jsp"/>
</head>
<body>
	<!--header-->
	<jsp:include page="placeholders/default-header.jsp"/>
	
	<div class="container">
		<div class="login">
			<form:form name="login-form" action="/shop-rest-api/api/perform-login" method="POST">
				<div class="col-md-6 login-do">
					<div class="login-mail">
						<input placeholder="Логін" type="text" name="username">
						<i class="glyphicon glyphicon-envelope"></i>
					</div>
					<div class="login-mail">
						<input placeholder="Пароль" type="password" name="password">
						<i class="glyphicon glyphicon-lock"></i>
					</div>
					<label class="hvr-skew-backward"><input type="submit" name="submit"
					value="Увійти"></label>
				</div>
				<div class="col-md-6 login-right">
					<h3>Новий акаунт</h3>
					<p><strong>Купити вишиванку</strong>&nbsp;– нелегке завдання, адже тут потрібно звертати увагу
					на якість тканини, значення орнаментів та кольорів. Якщо Ви прагнете знайти дійсно якісну сорочку,
					зверніть увагу на інтернет-магазин Бандерштат. Ми можемо гарантувати високу якість кожної вишиванки,
					адже співпрацюємо з перевіреними виробниками.</p>
					<p>У нас Ви зможете&nbsp;<strong>купити</strong>&nbsp;вишиванки у&nbsp;
					<strong>Львові дешево та сердито</strong>.</p>
					<a class=" hvr-skew-backward" href="register.html">Зареєструватись</a>
				</div>
				<div class="clearfix"></div>
			</form:form>
		</div>
	</div>
	
	<!--footer-->
	<jsp:include page="placeholders/footer.jsp"/>
</body>
</html>