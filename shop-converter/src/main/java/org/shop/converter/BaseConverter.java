package org.shop.converter;

import org.shop.model.ModelWithSimpleId;
import org.shop.resource.Resource;

/**
 * Base entity converter.
 * @author OlehZanevych
 *
 * @param <SOURCE> Entity
 * @param <TARGET> Resource
 */
public abstract class BaseConverter<SOURCE extends ModelWithSimpleId, TARGET extends Resource>
		extends AbstractConverter<SOURCE, TARGET> {
	
	@Override
	public TARGET convert(final SOURCE source, final TARGET target) {
		
		Long id = source.getId();
		if (id != null) {
			target.setId(id);
		}
		
		return target;
	}

}
