package org.shop.converter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.shop.model.ModelWithSimpleId;
import org.shop.resource.Resource;

/**
 * Abstract resource converter.
 * The reason I created this class is that I need to have
 * a convenient global mechanism to cascade update all child entities.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource
 * @param <ENTITY> Entity
 */
public abstract class AbstractResourceConverter<RESOURCE extends Resource, ENTITY extends ModelWithSimpleId>
		extends AbstractConverter<RESOURCE, ENTITY> {

	/**
     * Method for updating List of Entities.
     * @param resources List of Resources
     * @param entities List of existing Entities
     * @param parentEntityField Name of parent entity reference fields
     * @param parent Parent Entity
     */
	public void updateCollection(final List<RESOURCE> resources, final List<ENTITY> entities,
			final String parentEntityField, final ModelWithSimpleId parent) {
		
		List<RESOURCE> newResources;
		if (entities.isEmpty()) {
			newResources = resources;
		} else {
			newResources = new LinkedList<RESOURCE>();
			HashMap<Long, RESOURCE> resourceWithId = new HashMap<Long, RESOURCE>();
			for (RESOURCE resource: resources) {
				Long id = resource.getId();
				if (id != null) {
					resourceWithId.put(id, resource);
				} else {
					newResources.add(resource);
				}
			}
			if (resourceWithId.values().isEmpty()) {
				entities.clear();
				newResources = resources;
			} else {
				Iterator<ENTITY> entitiesIterator = entities.iterator();
				while (entitiesIterator.hasNext()) {
					ENTITY entity = entitiesIterator.next();
					Long id = entity.getId();
					RESOURCE resource = resourceWithId.get(id);
					if (resource != null) {
						convert(resource, entity);
						resourceWithId.remove(id);
					} else {
						entitiesIterator.remove();
					}
				}
				newResources.addAll(resourceWithId.values());
			}
		}
		List<ENTITY> newEntities = newResources.stream().map(i -> convert(i)).collect(Collectors.toList());
		if (!newEntities.isEmpty()) {
			for (ENTITY newEntity: newEntities) {
				try {
					FieldUtils.writeField(newEntity,  parentEntityField, parent, true);
				} catch (IllegalAccessException e) {
					throw new IllegalArgumentException(
							"Some problems with setting parent reference during update collection. Please, contact dev team for fixing this issue");
				}
			}
			entities.addAll(newEntities);
		}
	}

}
