package org.shop.converter.producer;

import org.shop.annotation.Converter;
import org.shop.converter.BaseConverter;
import org.shop.model.producer.Producer;
import org.shop.resource.producer.ProducerResource;

/**
 * Converter, that converts from Producer to ProducerResource.
 * @author OlehZanevych
 */
@Converter("producerConverter")
public class ProducerConverter extends BaseConverter<Producer, ProducerResource> {

	@Override
	public ProducerResource convert(final Producer source, final ProducerResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String imageUri = source.getImageUri();
		if (imageUri != null) {
			target.setImageUri(imageUri);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public ProducerResource convert(final Producer source) {
		return convert(source, new ProducerResource());
	}

}
