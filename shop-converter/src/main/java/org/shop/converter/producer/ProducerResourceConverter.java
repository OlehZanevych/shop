package org.shop.converter.producer;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractResourceConverter;
import org.shop.model.producer.Producer;
import org.shop.resource.producer.ProducerResource;

/**
 * Converter, that converts from ProducerResource to Producer.
 * @author OlehZanevych
 */
@Converter("producerResourceConverter")
public class ProducerResourceConverter extends AbstractResourceConverter<ProducerResource, Producer> {

	@Override
	public Producer convert(final ProducerResource source, final Producer target) {
		
		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Producer convert(final ProducerResource source) {
		return convert(source, new Producer());
	}

}
