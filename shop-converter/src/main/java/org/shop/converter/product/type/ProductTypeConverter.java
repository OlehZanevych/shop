package org.shop.converter.product.type;

import org.shop.annotation.Converter;
import org.shop.converter.BaseConverter;
import org.shop.model.product.type.ProductType;
import org.shop.resource.product.type.ProductTypeResource;

/**
 * Converter, that converts from ProductType to ProductTypeResource.
 * @author OlehZanevych
 */
@Converter("productTypeConverter")
public class ProductTypeConverter extends BaseConverter<ProductType, ProductTypeResource> {

	@Override
	public ProductTypeResource convert(final ProductType source, final ProductTypeResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public ProductTypeResource convert(final ProductType source) {
		return convert(source, new ProductTypeResource());
	}

}
