package org.shop.converter.product;

import org.shop.annotation.Converter;
import org.shop.converter.BaseConverter;
import org.shop.model.enumtype.category.Category;
import org.shop.model.producer.Producer;
import org.shop.model.product.Product;
import org.shop.model.product.type.ProductType;
import org.shop.resource.product.ProductResource;

/**
 * Converter, that converts from Product to ProductResource.
 * @author OlehZanevych
 */
@Converter("productConverter")
public class ProductConverter extends BaseConverter<Product, ProductResource> {

	@Override
	public ProductResource convert(final Product source, final ProductResource target) {
		super.convert(source, target);
		
		ProductType productType = source.getProductType();
		if (productType != null) {
			target.setProductTypeId(productType.getId());
		}
		
		Category category = source.getCategory();
		if (category != null) {
			target.setCategory(category);
		}
		
		Producer producer = source.getProducer();
		if (producer != null) {
			target.setProducerId(producer.getId());
		}
		
		Integer cost = source.getCost();
		if (cost != null) {
			target.setCost(cost);
		}
		
		String imageUri = source.getImageUri();
		if (imageUri != null) {
			target.setImageUri(imageUri);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public ProductResource convert(final Product source) {
		return convert(source, new ProductResource());
	}

}
