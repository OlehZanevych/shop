package org.shop.converter.product.type;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractResourceConverter;
import org.shop.model.product.type.ProductType;
import org.shop.resource.product.type.ProductTypeResource;

/**
 * Converter, that converts from ProductTypeResource to ProductType.
 * @author OlehZanevych
 */
@Converter("productTypeResourceConverter")
public class ProductTypeResourceConverter extends AbstractResourceConverter<ProductTypeResource, ProductType> {

	@Override
	public ProductType convert(final ProductTypeResource source, final ProductType target) {
		
		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public ProductType convert(final ProductTypeResource source) {
		return convert(source, new ProductType());
	}

}
