package org.shop.converter.product;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractResourceConverter;
import org.shop.model.enumtype.category.Category;
import org.shop.model.producer.Producer;
import org.shop.model.product.Product;
import org.shop.model.product.type.ProductType;
import org.shop.resource.product.ProductResource;

/**
 * Converter, that converts from ProductResource to Product.
 * @author OlehZanevych
 */
@Converter("productResourceConverter")
public class ProductResourceConverter extends AbstractResourceConverter<ProductResource, Product> {

	@Override
	public Product convert(final ProductResource source, final Product target) {
		
		Long productTypeId = source.getProductTypeId();
		if (productTypeId != null) {
			if (productTypeId == 0) {
				target.setProductType(null);
			} else {
				target.setProductType(new ProductType(productTypeId));
			}
		}
		
		Category category = source.getCategory();
		if (category != null) {
			if (category == Category.NULL) {
				target.setCategory(null);
			} else {
				target.setCategory(category);
			}
		}
		
		Long producerId = source.getProducerId();
		if (producerId != null) {
			if (producerId == 0) {
				target.setProducer(null);
			} else {
				target.setProducer(new Producer(producerId));
			}
		}
		
		Integer cost = source.getCost();
		if (cost != null) {
			if (cost == 0) {
				target.setCost(null);
			} else {
				target.setCost(cost);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Product convert(final ProductResource source) {
		return convert(source, new Product());
	}

}
