package org.shop.converter.feedbackmessage;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractResourceConverter;
import org.shop.resource.feedbackmessage.FeedbackMessageResource;
import org.shop.model.message.Message;

/**
 * Converter, that converts from FeedbackMessageResource to Message.
 * @author OlehZanevych
 */
@Converter("feedbackMessageResourceConverter")
public class FeedbackMessageResourceConverter extends AbstractResourceConverter<FeedbackMessageResource, Message> {

	@Override
	public Message convert(final FeedbackMessageResource source, final Message target) {
		
		String senderName = source.getSenderName();
		if (senderName != null) {
			if (senderName.isEmpty()) {
				target.setSenderName(null);
			} else {
				target.setSenderName(senderName);
			}
		}
		
		String senderEmail = source.getSenderEmail();
		if (senderEmail != null) {
			if (senderEmail.isEmpty()) {
				target.setSenderEmail(null);
			} else {
				target.setSenderEmail(senderEmail);
			}
		}
		
		String subject = source.getSubject();
		if (subject != null) {
			if (subject.isEmpty()) {
				target.setSubject(null);
			} else {
				target.setSubject(subject);
			}
		}
		
		String description = source.getDescription();
		if (description != null) {
			if (description.isEmpty()) {
				target.setDescription(null);
			} else {
				target.setDescription(description);
			}
		}
		
		return target;
	}

	@Override
	public Message convert(final FeedbackMessageResource source) {
		return convert(source, new Message());
	}

}
