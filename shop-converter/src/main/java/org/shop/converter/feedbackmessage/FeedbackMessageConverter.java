package org.shop.converter.feedbackmessage;

import org.shop.annotation.Converter;
import org.shop.converter.BaseConverter;
import org.shop.resource.feedbackmessage.FeedbackMessageResource;
import org.shop.model.message.Message;

/**
 * Converter, that converts from Message to FeedbackMessageResource.
 * @author OlehZanevych
 */
@Converter("feedbackMessageConverter")
public class FeedbackMessageConverter extends BaseConverter<Message, FeedbackMessageResource> {

	@Override
	public FeedbackMessageResource convert(final Message source, final FeedbackMessageResource target) {
		super.convert(source, target);
		
		String senderName = source.getSenderName();
		if (senderName != null) {
			target.setSenderName(senderName);
		}
		
		String senderEmail = source.getSenderEmail();
		if (senderEmail != null) {
			target.setSenderEmail(senderEmail);
		}
		
		String subject = source.getSubject();
		if (subject != null) {
			target.setSubject(subject);
		}
		
		String description = source.getDescription();
		if (description != null) {
			target.setDescription(description);
		}
		
		return target;
	}

	@Override
	public FeedbackMessageResource convert(final Message source) {
		return convert(source, new FeedbackMessageResource());
	}

}
