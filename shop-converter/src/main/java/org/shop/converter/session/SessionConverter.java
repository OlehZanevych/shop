package org.shop.converter.session;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractConverter;
import org.shop.model.session.Session;
import org.shop.resource.session.SessionResource;

/**
 * Session converter.
 * @author OlehZanevych
 *
 */
@Converter("sessionConverter")
public class SessionConverter extends AbstractConverter<Session, SessionResource> {
	
	@Override
	public SessionResource convert(final Session source, final SessionResource target) {
		
		Long userId = source.getUserId();
		if (userId != null) {
			target.setUserId(userId);
		}
		
		return target;
	}

	@Override
	public SessionResource convert(final Session source) {
		return convert(source, new SessionResource());
	}

}
