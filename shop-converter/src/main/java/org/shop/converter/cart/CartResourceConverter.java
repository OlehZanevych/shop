package org.shop.converter.cart;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractResourceConverter;
import org.shop.model.product.Product;
import org.shop.model.user.User;
import org.shop.model.cart.Cart;
import org.shop.resource.cart.CartResource;

/**
 * Converter, that converts from CartResource to Cart.
 * @author OlehZanevych
 */
@Converter("cartResourceConverter")
public class CartResourceConverter extends AbstractResourceConverter<CartResource, Cart> {

	@Override
	public Cart convert(final CartResource source, final Cart target) {
		
		Long productId = source.getProductId();
		if (productId != null) {
			if (productId == 0) {
				target.setProduct(null);
			} else {
				target.setProduct(new Product(productId));
			}
		}
		
		Short count = source.getCount();
		if (count != null) {
			if (count == 0) {
				target.setCount(null);
			} else {
				target.setCount(count);
			}
		}
		
		Long userId = source.getUserId();
		if (userId != null) {
			if (userId == 0) {
				target.setUser(null);
			} else {
				target.setUser(new User(userId));
			}
			
		}
		
		return target;
	}

	@Override
	public Cart convert(final CartResource source) {
		return convert(source, new Cart());
	}
}
