package org.shop.converter.cart;

import org.shop.annotation.Converter;
import org.shop.converter.BaseConverter;
import org.shop.model.enumtype.category.Category;
import org.shop.model.producer.Producer;
import org.shop.model.cart.Cart;
import org.shop.model.product.Product;
import org.shop.model.product.type.ProductType;
import org.shop.model.user.User;
import org.shop.resource.cart.CartResource;

/**
 * Converter, that converts from Cart to CartResource.
 * @author OlehZanevych
 */
@Converter("cartConverter")
public class CartConverter extends BaseConverter<Cart, CartResource> {

	@Override
	public CartResource convert(final Cart source, final CartResource target) {
		super.convert(source, target);
		
		Product product = source.getProduct();
		if (product != null) {
			target.setProductId(product.getId());
			
			ProductType productType = product.getProductType();
			if (productType != null) {
				target.setProductTypeId(productType.getId());
				target.setProductTypeName(productType.getName());
			}
			
			Category category = product.getCategory();
			if (category != null) {
				target.setCategory(category);
			}
			
			Producer producer = product.getProducer();
			if (producer != null) {
				target.setProducerId(producer.getId());
				target.setProducerName(producer.getName());
			}
			
			Integer cost = product.getCost();
			if (cost != null) {
				target.setCost(cost);
			}
			
			String imageUri = product.getImageUri();
			if (imageUri != null) {
				target.setImageUri(imageUri);
			}
			
			String info = product.getInfo();
			if (info != null) {
				target.setInfo(info);
			}
		}
		
		Short count = source.getCount();
		if (count != null) {
			target.setCount(count);
		}
		
		User user = source.getUser();
		if (user != null) {
			target.setUserId(user.getId());
		}
		
		return target;
	}

	@Override
	public CartResource convert(final Cart source) {
		return convert(source, new CartResource());
	}

}
