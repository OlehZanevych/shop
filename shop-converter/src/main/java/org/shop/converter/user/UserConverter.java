package org.shop.converter.user;

import org.shop.annotation.Converter;
import org.shop.converter.BaseConverter;
import org.shop.model.enumtype.role.Role;
import org.shop.model.user.User;
import org.shop.resource.user.UserResource;

/**
 * Converter, that converts from User to UserResource.
 * @author OlehZanevych
 */
@Converter("userConverter")
public class UserConverter extends BaseConverter<User, UserResource> {

	@Override
	public UserResource convert(final User source, final UserResource target) {
		
		super.convert(source, target);
		
		String login = source.getLogin();
		if (login != null) {
			target.setLogin(login);
		}
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			target.setSurname(surname);
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			target.setFirstName(firstName);
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			target.setMiddleName(middleName);
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		Role role = source.getRole();
		if (role != null) {
			target.setRole(role);
		}
		
		return target;
	}

	@Override
	public UserResource convert(final User source) {
		return convert(source, new UserResource());
	}

}
