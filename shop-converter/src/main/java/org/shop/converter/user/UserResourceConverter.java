package org.shop.converter.user;

import javax.annotation.Resource;

import org.shop.annotation.Converter;
import org.shop.converter.AbstractResourceConverter;
import org.shop.model.enumtype.role.Role;
import org.shop.model.user.User;
import org.shop.resource.user.UserResource;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Converter, that converts from UserResource to User.
 * @author OlehZanevych
 */
@Converter("userResourceConverter")
public class UserResourceConverter extends AbstractResourceConverter<UserResource, User> {
	
	@Resource(name = "bCryptPasswordEncoder")
    private PasswordEncoder passwordEncoder;

	@Override
	public User convert(final UserResource source, final User target) {
		
		String login = source.getLogin();
		if (login != null) {
			if (login.isEmpty()) {
				target.setLogin(null);
			} else {
				target.setLogin(login);
			}
		}
		
		String password = source.getPassword();
		if (password != null) {
			if (password.isEmpty()) {
				target.setPasswordHash(null);
			} else {
				target.setPasswordHash(passwordEncoder.encode(password));
			}
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			if (surname.isEmpty()) {
				target.setSurname(null);
			} else {
				target.setSurname(surname);
			}
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			if (firstName.isEmpty()) {
				target.setFirstName(null);
			} else {
				target.setFirstName(firstName);
			}
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			if (middleName.isEmpty()) {
				target.setMiddleName(null);
			} else {
				target.setMiddleName(middleName);
			}
		}
		
		String email = source.getEmail();
		if (email != null) {
			if (email.isEmpty()) {
				target.setEmail(null);
			} else {
				target.setEmail(email);
			}
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			if (phone.isEmpty()) {
				target.setPhone(null);
			} else {
				target.setPhone(phone);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		Role role = source.getRole();
		if (role != null) {
			if (role == Role.NULL) {
				target.setRole(null);
			} else {
				target.setRole(role);
			}
		}
		
		return target;
	}

	@Override
	public User convert(final UserResource source) {
		return convert(source, new User());
	}

}
