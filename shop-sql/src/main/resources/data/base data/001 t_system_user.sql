INSERT INTO t_system_users(id, login, password_hash, surname, first_name, middle_name, email, phone, info, role) VALUES 
	(1, 'admin', '$2a$10$IM24p5bo0Ensum.R7/uoN.UWWwT76hIGPKfeJq059ExG4YEFF4fbi', 'Zanevych', 'Oleh', 'Bohdanovych', 'Oleh.Zanevych@gmail.com', NULL, NULL, 'ADMIN'), 
	(2, 'customer', '$2a$10$IM24p5bo0Ensum.R7/uoN.UWWwT76hIGPKfeJq059ExG4YEFF4fbi', 'Test', 'Test', 'Testovych', 'Test@gmail.com', NULL, NULL, 'CUSTOMER'),
	(3, 'ban', '$2a$10$IM24p5bo0Ensum.R7/uoN.UWWwT76hIGPKfeJq059ExG4YEFF4fbi', 'Test2', 'Tes2t', 'Testovych2', 'Tes2t@gmail.com', NULL, NULL, 'BAN'),
	(4, 'moderator', '$2a$10$IM24p5bo0Ensum.R7/uoN.UWWwT76hIGPKfeJq059ExG4YEFF4fbi', 'Test3', 'Test3', 'Testovych3', 'Test3@gmail.com', NULL, NULL, 'MODERATOR');
SELECT setval('t_system_users_id_seq', 4);
