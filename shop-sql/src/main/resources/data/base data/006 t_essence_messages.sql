INSERT INTO t_essence_messages (id, sender_name, sender_email, subject, description) VALUES 
	(1, 'Олег', 'Oleh.Zanevych@gmail.com', 'Тестове повідомлення 1', 'Тестова інформація для тестового повідомдення 1'),
	(2, 'Андрій', 'Andriy@gmail.com', 'Тестове повідомлення 2', 'Тестова інформація для тестового повідомдення 2'),
	(3, 'Богдан', 'Bohdan@gmail.com', 'Тестове повідомлення 3', 'Тестова інформація для тестового повідомдення 3'),
	(4, 'Іван', 'Ivan@gmail.com', 'Тестове повідомлення 4', 'Тестова інформація для тестового повідомдення 4');
SELECT setval('t_essence_messages_id_seq', 4);
