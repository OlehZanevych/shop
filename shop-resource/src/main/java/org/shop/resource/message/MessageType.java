package org.shop.resource.message;

/**
 * Type of message, that we can send to response.
 * @author OlehZanevych
 *
 */
public enum MessageType {
	ERROR, INFO;
}
