package org.shop.resource;

/**
 * Base Resource with id.
 * @author OlehZanevych
 */
public abstract class Resource {

	protected Long id;
	
	/**
	 * Default constructor with no parameters.
	 */
	protected Resource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	protected Resource(final Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Resource other = (Resource) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Resource [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
}
