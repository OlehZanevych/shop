package org.shop.resource.request;

import java.util.List;

/**
 * PagedResultResource, that is sent to response,
 * when we need to have pagination response.
 * @author OlehZanevych
 *
 * @param <R> Generic object class of resource
 */
public class PagedResultResource<R> {

    private Integer offset;
    private Integer limit;
    private long count;
    private List<R> resources;

   /**
     * Constructor for all arguments.
     * @param offset offset
     * @param limit limit
     * @param count count
     * @param resources resources
     */
	public PagedResultResource(final Integer offset, final Integer limit, final long count,
			final List<R> resources) {
		
		this.offset = offset;
		this.limit = limit;
		this.count = count;
		this.resources = resources;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(final Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(final Integer limit) {
		this.limit = limit;
	}

	public long getCount() {
		return count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public List<R> getResources() {
		return resources;
	}

	public void setResources(final List<R> resources) {
		this.resources = resources;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (count ^ (count >>> 32));
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		result = prime * result + ((offset == null) ? 0 : offset.hashCode());
		result = prime * result + ((resources == null) ? 0 : resources.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PagedResultResource<?> other = (PagedResultResource<?>) obj;
		if (count != other.count) {
			return false;
		}
		if (limit == null) {
			if (other.limit != null) {
				return false;
			}
		} else if (!limit.equals(other.limit)) {
			return false;
		}
		if (offset == null) {
			if (other.offset != null) {
				return false;
			}
		} else if (!offset.equals(other.offset)) {
			return false;
		}
		if (resources == null) {
			if (other.resources != null) {
				return false;
			}
		} else if (!resources.equals(other.resources)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PagedResultResource [offset=");
		builder.append(offset);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", count=");
		builder.append(count);
		builder.append(", resources=");
		builder.append(resources);
		builder.append("]");
		return builder.toString();
	}
	
}
