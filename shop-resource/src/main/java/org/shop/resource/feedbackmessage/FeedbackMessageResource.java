package org.shop.resource.feedbackmessage;

import org.shop.annotation.CrudableResource;
import org.shop.resource.APIResource;

/**
 * Resource for web layer, that describes Message.
 * @author OlehZanevych
 */
@CrudableResource
public class FeedbackMessageResource extends APIResource {
	
	private String senderName;
	
	private String senderEmail;
	
	private String subject;
	
	private String description;
	
	/**
	 * Default constructor with no parameters.
	 */
	public FeedbackMessageResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public FeedbackMessageResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return "/feedbackmessages";
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(final String senderName) {
		this.senderName = senderName;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(final String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((senderEmail == null) ? 0 : senderEmail.hashCode());
		result = prime * result + ((senderName == null) ? 0 : senderName.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FeedbackMessageResource other = (FeedbackMessageResource) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (senderEmail == null) {
			if (other.senderEmail != null) {
				return false;
			}
		} else if (!senderEmail.equals(other.senderEmail)) {
			return false;
		}
		if (senderName == null) {
			if (other.senderName != null) {
				return false;
			}
		} else if (!senderName.equals(other.senderName)) {
			return false;
		}
		if (subject == null) {
			if (other.subject != null) {
				return false;
			}
		} else if (!subject.equals(other.subject)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeedbackMessageResource [senderName=");
		builder.append(senderName);
		builder.append(", senderEmail=");
		builder.append(senderEmail);
		builder.append(", subject=");
		builder.append(subject);
		builder.append(", description=");
		builder.append(description);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
