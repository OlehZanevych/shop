package org.shop.resource.session;

import java.io.Serializable;

/**
 * Session Resource class.
 * @author OlehZanevych
 */
public class SessionResource implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userId;
	
	/**
	 * Default constructor with no parameters.
	 */
	public SessionResource() {
		
	}

	/**
	 * Constructor with userId.
	 * @param userId User identifier
	 */
	public SessionResource(final Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SessionResource other = (SessionResource) obj;
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SessionResource [userId=");
		builder.append(userId);
		builder.append("]");
		return builder.toString();
	}
}
