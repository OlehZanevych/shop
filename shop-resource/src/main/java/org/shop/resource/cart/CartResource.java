package org.shop.resource.cart;

import org.shop.annotation.CrudableResource;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.APIResource;

/**
 * Resource for web layer, that describes Cart.
 * @author OlehZanevych
 */
@CrudableResource
public class CartResource extends APIResource {
	
	private Long productId;
	
	private Short count;
	
	private Long userId;
	
	private Long productTypeId;
	
	private String productTypeName;
	
	private Category category;
	
	private Long producerId;
	
	private String producerName;
	
	private Integer cost;
	
	private String imageUri;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public CartResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CartResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return "/carts";
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(final Long productId) {
		this.productId = productId;
	}

	public Short getCount() {
		return count;
	}

	public void setCount(final Short count) {
		this.count = count;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	public Long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(final Long productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(final String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(final Category category) {
		this.category = category;
	}

	public Long getProducerId() {
		return producerId;
	}

	public void setProducerId(final Long producerId) {
		this.producerId = producerId;
	}

	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(final String producerName) {
		this.producerName = producerName;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(final Integer cost) {
		this.cost = cost;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(final String imageUri) {
		this.imageUri = imageUri;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((count == null) ? 0 : count.hashCode());
		result = prime * result + ((imageUri == null) ? 0 : imageUri.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((producerId == null) ? 0 : producerId.hashCode());
		result = prime * result + ((producerName == null) ? 0 : producerName.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((productTypeId == null) ? 0 : productTypeId.hashCode());
		result = prime * result + ((productTypeName == null) ? 0 : productTypeName.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CartResource other = (CartResource) obj;
		if (category != other.category) {
			return false;
		}
		if (cost == null) {
			if (other.cost != null) {
				return false;
			}
		} else if (!cost.equals(other.cost)) {
			return false;
		}
		if (count == null) {
			if (other.count != null) {
				return false;
			}
		} else if (!count.equals(other.count)) {
			return false;
		}
		if (imageUri == null) {
			if (other.imageUri != null) {
				return false;
			}
		} else if (!imageUri.equals(other.imageUri)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (producerId == null) {
			if (other.producerId != null) {
				return false;
			}
		} else if (!producerId.equals(other.producerId)) {
			return false;
		}
		if (producerName == null) {
			if (other.producerName != null) {
				return false;
			}
		} else if (!producerName.equals(other.producerName)) {
			return false;
		}
		if (productId == null) {
			if (other.productId != null) {
				return false;
			}
		} else if (!productId.equals(other.productId)) {
			return false;
		}
		if (productTypeId == null) {
			if (other.productTypeId != null) {
				return false;
			}
		} else if (!productTypeId.equals(other.productTypeId)) {
			return false;
		}
		if (productTypeName == null) {
			if (other.productTypeName != null) {
				return false;
			}
		} else if (!productTypeName.equals(other.productTypeName)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CartResource [productId=");
		builder.append(productId);
		builder.append(", count=");
		builder.append(count);
		builder.append(", userId=");
		builder.append(userId);
		builder.append(", productTypeId=");
		builder.append(productTypeId);
		builder.append(", productTypeName=");
		builder.append(productTypeName);
		builder.append(", category=");
		builder.append(category);
		builder.append(", producerId=");
		builder.append(producerId);
		builder.append(", producerName=");
		builder.append(producerName);
		builder.append(", cost=");
		builder.append(cost);
		builder.append(", imageUri=");
		builder.append(imageUri);
		builder.append(", info=");
		builder.append(info);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
