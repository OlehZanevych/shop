package org.shop.resource;

import java.text.MessageFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Abstract class, that all API resources need to implement.
 * @author OlehZanevych
 */
public abstract class APIResource extends Resource {
	
	/**
	 * Default constructor with no parameters.
	 */
	protected APIResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	protected APIResource(final Long id) {
		super(id);
	}
	
	/**
	 * Method, that is used for getting uri for list (multiple resources).
	 * @return root uri.
	 */
	@JsonIgnore
	public abstract String getRootUri();
	
	/**
	 * Method for getting unique uri for all resources.
	 * @return uri of string representation.
	 */
	public String getUri() {
		if (id == null) {
			return null;
		}
		return MessageFormat.format("{0}/{1,number,#}", getRootUri(), id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("APIResource [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
}
