package org.shop.resource.product;

import org.shop.annotation.CrudableResource;
import org.shop.model.enumtype.category.Category;
import org.shop.resource.APIResource;

/**
 * Resource for web layer, that describes Product.
 * @author OlehZanevych
 */
@CrudableResource
public class ProductResource extends APIResource {
	
	private Long productTypeId;
	
	private Category category;
	
	private Long producerId;
	
	private Integer cost;
	
	private String imageUri;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public ProductResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public ProductResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return "/products";
	}

	public Long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(final Long productTypeId) {
		this.productTypeId = productTypeId;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(final Category category) {
		this.category = category;
	}

	public Long getProducerId() {
		return producerId;
	}

	public void setProducerId(final Long producerId) {
		this.producerId = producerId;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(final Integer cost) {
		this.cost = cost;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(final String imageUri) {
		this.imageUri = imageUri;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((imageUri == null) ? 0 : imageUri.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((producerId == null) ? 0 : producerId.hashCode());
		result = prime * result + ((productTypeId == null) ? 0 : productTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProductResource other = (ProductResource) obj;
		if (category != other.category) {
			return false;
		}
		if (cost == null) {
			if (other.cost != null) {
				return false;
			}
		} else if (!cost.equals(other.cost)) {
			return false;
		}
		if (imageUri == null) {
			if (other.imageUri != null) {
				return false;
			}
		} else if (!imageUri.equals(other.imageUri)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (producerId == null) {
			if (other.producerId != null) {
				return false;
			}
		} else if (!producerId.equals(other.producerId)) {
			return false;
		}
		if (productTypeId == null) {
			if (other.productTypeId != null) {
				return false;
			}
		} else if (!productTypeId.equals(other.productTypeId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductResource [productTypeId=");
		builder.append(productTypeId);
		builder.append(", category=");
		builder.append(category);
		builder.append(", producerId=");
		builder.append(producerId);
		builder.append(", cost=");
		builder.append(cost);
		builder.append(", imageUri=");
		builder.append(imageUri);
		builder.append(", info=");
		builder.append(info);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
