package org.shop.resource.product.type;

import org.shop.annotation.CrudableResource;
import org.shop.resource.APIResource;

/**
 * Resource for web layer, that describes ProductType.
 * @author OlehZanevych
 */
@CrudableResource
public class ProductTypeResource extends APIResource {
	
	private String name;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public ProductTypeResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public ProductTypeResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return "/producttypes";
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProductTypeResource other = (ProductTypeResource) obj;
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductTypeResource [name=");
		builder.append(name);
		builder.append(", info=");
		builder.append(info);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
