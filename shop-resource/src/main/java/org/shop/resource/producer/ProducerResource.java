package org.shop.resource.producer;

import org.shop.annotation.CrudableResource;
import org.shop.resource.APIResource;

/**
 * Resource for web layer, that describes Producer.
 * @author OlehZanevych
 */
@CrudableResource
public class ProducerResource extends APIResource {
	
	private String name;
	
	private String imageUri;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public ProducerResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public ProducerResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return "/producers";
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(final String imageUri) {
		this.imageUri = imageUri;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((imageUri == null) ? 0 : imageUri.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProducerResource other = (ProducerResource) obj;
		if (imageUri == null) {
			if (other.imageUri != null) {
				return false;
			}
		} else if (!imageUri.equals(other.imageUri)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProducerResource [name=");
		builder.append(name);
		builder.append(", imageUri=");
		builder.append(imageUri);
		builder.append(", info=");
		builder.append(info);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
